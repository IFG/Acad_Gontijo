<?php

require_once '../ADOs/TreinoExercicioAdo.php';
require_once '../ADOs/TipoDeTreinoAdo.php';

$gmtDate = gmdate("D, d M Y H:i:s");
header("Expires: {$gmtDate} GMT");
header("Last-Modified: {$gmtDate} GMT");
header("Cache-Contro: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type; text/html; charset=utf-8");

Class ExercicioAjax {

    public function __construct() {
        $trenId = $_POST['trenId'];

        if (empty(trim($trenId)) || $trenId == '-1') {
            $arrayResposta[0] = "erro";
        }

        $treinoExercicioAdo = new TreinoExercicioAdo();
        $buscou = $exercicioModel = $treinoExercicioAdo->buscaExerciciosPorTreino($trenId);
        
        $arrayResposta[0] = "add";
        $arrayResposta[] = -1;
        
        if ($buscou) {
            $arrayResposta[] = "Escolha um Exercício...";
            foreach ($exercicioModel as $exercicioModel) {
                $text = "Nome: " . $exercicioModel->exer_nome . " - Tipo de Treino: " . $exercicioModel->tptr_nome;
                $arrayResposta[] = $exercicioModel->exer_id;
                $arrayResposta[] = $text;
            }
        } else {
            $arrayResposta[] = "Escolha um Exercício...";
        }
        
        echo implode("|", $arrayResposta);
    }

}

$ExercicioAjax = new ExercicioAjax();
unset($ExercicioAjax);

