<?php

class MontaHTML {

    /**
     * Cria uma linha de classe "linhaFileldset" para um input do tipo hidden.
     * @param array $dadosInput Array de dados contendo: nome, valor do input 
     * e identificador, este último não é obrigatório.
     * @return string String com o código HTML da linha do hidden.
     * @example $dadosfieldset[] = array("name" => "fieldset", "value" => "", "id" => "identificador");
     */
    function montaInputHidden(array $dadosInput) {
        $input = NULL;
        $id = null;

        if (isset($dadosInput['id'])) {
            if (is_null($dadosInput['id'])) {
                //se vazio não precisa fazer nada
            } else {
                $id = "id=\"{$dadosInput['id']}\" ";
            }
        }

        $input .= "<input type=\"hidden\" {$id} "
                . "name=\"{$dadosInput['name']}\" value=\"{$dadosInput['value']}\" />\n";

        return $input;
    }

    private function montaInputParte1(Array $dadosFieldset) {
        $fieldset = NULL;

        $disabled = null;
        if (isset($dadosFieldset['disabled']) && $dadosFieldset['disabled']) {
            $disabled = " disabled ";
        }

        $placeholder = null;
        if (isset($dadosFieldset['placeholder']) && !is_null($dadosFieldset['placeholder'])) {
            $placeholder = "placeholder=\"{$dadosFieldset['placeholder']}\" ";
        }

        $class = null;
        if (isset($dadosFieldset['class'])) {
            if (is_null($dadosFieldset['class'])) {
                //continua
            } else {
                $class = "class=\"{$dadosFieldset['class']}\" ";
            }
        }

        $maxlength = null;
        if (isset($dadosFieldset['maxlength'])) {
            if (is_null($dadosFieldset['maxlength'])) {
                //continua
            } else {
                $maxlength = "maxlength=\"{$dadosFieldset['maxlength']}\" ";
            }
        }

        //cria e limpa uma variável para guardar o código com o identificador.
        $id = null;
        if (isset($dadosFieldset['id'])) {
            if (is_null($dadosFieldset['id'])) {
                //se vazio não precisa fazer nada
            } else {
                $id = "id=\"{$dadosFieldset['id']}\" ";
            }
        }

        $fieldset .= "<label>{$dadosFieldset['label']}</label>\n";
        $fieldset .= "<input type=\"{$dadosFieldset['type']}\" "
                . "name=\"{$dadosFieldset['name']}\" "
                . "value=\"{$dadosFieldset['value']}\" "
                . $placeholder . $disabled . $class . $maxlength
                . $id . "/>\n";
        return $fieldset;
    }

    /**
     * Cria uma linha de classe "linhaFileldset" com label e input do tipo texto.
     * @param array $dadosFieldset Array de dados contendo: texto do label, 
     * tipo, nome, valor do input, placeholder, valor lógico para desabilitar ou não,
     * uma classe para CSS/JS podendo ser usada ou não, atributo maxlength para
     * determinar a quantidade máxima de caracteres permitidos, podendo ser usado
     * ou não e identificador, podendo ser nulo.
     * @return string String com o código HTML da linha de entrada de dados.
     * @example $dadosfieldset = array("label" => "Nome do Label", "type" => "text", "name" => "fieldset", "value" => "", "placeholder" => "", "disabled" => true/false, "class" => "classeCss/classeJS", "maxlength" => "null/quantidade(5)", "id" => "identificador");
     */
    function montaInput(array $dadosFieldset) {
        $fieldset = $this->montaInputParte1($dadosFieldset);

        return $fieldset;
    }

    function montaInputComCodigoExtra($codigoExtra, array $dadosFieldset) {
        $fieldset = $this->montaInputParte1($dadosFieldset);
        $fieldset .= $codigoExtra;

        return $fieldset;
    }

    /**
     * Cria uma linha de classe "linhaFileldset" com label e input para dado do tipo telefone.
     * @param array $dadosFieldset Array de dados contendo: texto do label, 
     * nome, valor do input e valor lógico para desabilitar ou não.
     * @return string String com o código HTML da linha de entrada de dados.
     * @example $dadosfieldset = array("label" => "Telefone", "name" => "telefone", "value" => "", "disabled" => true/false);
     */
    function montaInputDeTelefone(array $dadosFieldset) {
        $fieldset = null;
        $disabled = null;

        // CSB - Adicionei a verificação do parâmetro "disabled" para evitar erro notice "Undefined index: disabled"
        if (isset($dadosFieldset['disabled']) && $dadosFieldset['disabled'] === true) {
            //$disabled = "disabled=\"disabled\" ";
            $disabled = " disabled ";
        }
        $fieldset .= "<label>{$dadosFieldset['label']}</label>\n";
        $fieldset .= "<input type=\"text\" "
                . "name=\"{$dadosFieldset['name']}\" value=\"{$dadosFieldset['value']}\" "
                . " class=\"mascaraFone\" size=\"16\" "
                . "{$disabled} />\n";

        return $fieldset;
    }

    /**
     * Cria uma linha de classe "linhaFileldset" com label e input para dado do 
     * tipo data.
     * @param array $dadosInputData Array de dados contendo: texto do label, 
     * nome, valor do input e valor lógico para desabilitar ou não.
     * @return string String com o código HTML da linha de entrada de dados.
     * @example $dadosfieldset = array("label" => "Fieldset Teste", "name" => "fieldset", "value" => "", "disabled" => true/false,"placeholder"=>"Informe algo...");
     */
    function montaInputDeData(array $dadosInputData) {
        $fieldset = NULL;
        $disabled = null;

        // CSB - Adicionei a verificação do parâmetro "disabled" para evitar erro notice "Undefined index: disabled"
        if (isset($dadosInputData['disabled']) && $dadosInputData['disabled'] === true) {
            $disabled = " disabled ";
        }

        // EPC - 26/09/216 - Adicionei a verificação do parâmetro "placeholder" para evitar erro notice "Undefined index: disabled"
        $placeholder = null;
        if (isset($dadosInputData['placeholder'])) {
            $placeholder = $dadosInputData["placeholder"];
        }

        $fieldset .= "<label>{$dadosInputData['label']}</label>\n";
        $fieldset .= "<input type=\"text\" "
                . "name=\"{$dadosInputData['name']}\" value=\"{$dadosInputData['value']}\" "
                . " class=\"datePicker mascaraData\" size=\"16\" "
                . "{$disabled} placeholder=\"{$placeholder}\" />\n";

        return $fieldset;
    }

    /**
     * Cria uma linha de classe "linhaFileldset" com label e input para dado do tipo CEP.
     * @param array $dadosFieldset Array de dados contendo: texto do label, 
     * identificador do input, nome e valor do input.
     * @return string String com o código HTML da linha de entrada de dados.
     * @example $dadosfieldset = array("label" => "Fieldset Teste", "id" => "testeId", "name" => "fieldset", "value" => "", "disabled" => true/false);
     */
    function montaInputDeCep(array $dadosFieldset) {
        $fieldset = NULL;
        $disabled = null;

        // CSB - Adicionei a verificação do parâmetro "disabled" para evitar erros do tipo "notice"
        if (isset($dadosFieldset['disabled']) && $dadosFieldset['disabled'] === true) {
            //$disabled = "disabled=\"disabled\" ";
            $disabled = " disabled ";
        }

        // CSB - Adicionei a verificação do parâmetro "id" para que, caso o programador
        // não envie um ID para o campo, usar o valor do parâmetro "name" no lugar
        // que serve perfeitamente, já que geralmente são iguais, assim evitando 
        // erros do tipo "notice" também
        if (!isset($dadosFieldset['id']) || is_null($dadosFieldset['id'])) {
            $dadosFieldset['id'] = $dadosFieldset['name'];
        }

        $fieldset .= "<label>{$dadosFieldset['label']}</label>\n";
        $fieldset .= "<input type=\"text\" id=\"{$dadosFieldset['id']}\" "
                . "name=\"{$dadosFieldset['name']}\" value=\"{$dadosFieldset['value']}\" "
                . "class=\"\" maxlength=\"8\" size=\"16\" "
                . "{$disabled} />\n";

        return $fieldset;
    }

    /**
     * Cria uma linha de classe "linhaFileldset" com label e input para dado do 
     * tipo hora.
     * @param array $dadosFieldset Array de dados contendo: texto do label, 
     * identificador do input, nome, valor do input, funcao JS onblur que é 
     * executada quando perde o foco do campo(input), podendo ser null e valor lógico para
     * desabilitar ou não. 
     * @return string String com o código HTML da linha de entrada de dados.
     * @example $dadosfieldset = array("label" => "Fieldset Teste", "id" => "testeId", "name" => "fieldset", "value" => "", "onblur" => "nomeDaFuncaoJS( )", "disabled" => true/false,"placeholder"=>"Informe algo...");
     */
    function montaInputDeHora(array $dadosFieldset) {
        $fieldset = NULL;
        $disabled = null;

        // CSB - Adicionei a verificação do parâmetro "disabled" para evitar erro notice "Undefined index: disabled"
        if (isset($dadosFieldset['disabled']) && $dadosFieldset['disabled'] === true) {
            //$disabled = "disabled=\"disabled\" ";
            $disabled = " disabled ";
        }

        // EPC - 26/09/216 - Adicionei a verificação do parâmetro "placeholder" para evitar erro notice "Undefined index: disabled"
        $placeholder = null;
        if (isset($dadosFieldset['placeholder'])) {
            $placeholder = $dadosFieldset["placeholder"];
        }

        $onblur = null;
        if (is_null($dadosFieldset['onblur'])) {
            //continua
        } else {
            $onblur = "onblur=\"{$dadosFieldset['onblur']}\"";
        }
        $fieldset .= "<label>{$dadosFieldset['label']}</label>\n";
        $fieldset .= "<input type=\"text\" id=\"{$dadosFieldset['id']}\" "
                . "name=\"{$dadosFieldset['name']}\" value=\"{$dadosFieldset['value']}\" "
                . "{$onblur} class=\"mascaraHora\" size=\"16\" "
                . "{$disabled} placeholder=\"{$placeholder}\" />\n";

        return $fieldset;
    }

    /**
     * Este método tem como objetivo padronizar a criação de Textarea.
     * @param array $dadosTextArea
     * Recebe como parâmetro um array de dados contendo: Label, Nome, Texto e o identificador desasbled ()true ou false)
     * @return string
     * retorna uma string contendo todos os dados do Teextarea.
     * @example $dadosTextArea = array("label" => "TextArea Teste", "name" => "TextArea Teste", "texto" => "");
     */
    function montaTextArea(array $dadosTextArea, $class = null, $rows = 3) {
        $disabled = null;

        if (isset($dadosTextArea["disabled"])) {
            if ($dadosTextArea["disabled"]) {
                $disabled = " disabled ";
            }
        }

        $textArea = "<label>{$dadosTextArea['label']}</label>";
        $textArea .= "<textarea class=\"{$class}\" rows=\"{$rows}\" name=\"{$dadosTextArea['name']}\" {$disabled}>{$dadosTextArea['texto']}";
        $textArea .= "</textarea>";

        return $textArea;
    }

    private function montaComboboxParte1(array $dadosCombo, $textoOpcaoPadrao, $onChange = null/* EPC - 15/09/2016 - PASSEI PARA DENTRO DO ARRAY PQ FICA MELHOR , $disabled = false */, $class) {
        $combobox = NULL;

        $disabled = null;
        if (isset($dadosCombo["disabled"])) {
            if ($dadosCombo["disabled"]) {
                //$disabled = " disabled=\"disabled\"";
                $disabled = " disabled ";
            }
        }

        $combobox .= "<label>{$dadosCombo['label']}</label>\n";
        $combobox .= "<select class=\"{$class}\"  name=\"{$dadosCombo['name']}\" id=\"{$dadosCombo['name']}\" $onChange $disabled>\n";
        if (is_null($textoOpcaoPadrao)) {
            //não faz nada qndo o texto vem nnulo, ou seja não monta a option padrão.
        } else {
            $combobox .= "<option value='-1'>" . $textoOpcaoPadrao . "</option>\n";
        }

        if (is_array($dadosCombo['options'])) {
            foreach ($dadosCombo['options'] as $option) {
                $selected = null;
                if (isset($option['selected']) && $option['selected']) {
                    //$selected = " selected='selected' ";
                    $selected = " selected ";
                }

                $combobox .= "<option value='{$option['value']}'{$selected}>{$option['text']}</option>\n";
            }
        }

        $combobox .= "</select>\n";
        return $combobox;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * ComboBoxes.
     * @param array $dadosCombo
     * Recebe como parâmetro um array de dados contendo: Label, Nome, Disabled (true ou false), e um array 
     * interno para montagem das options contendo: Valor e Texto e se a opção 
     * deve vir selecionada ou não.
     * @example $dadoscombo =  array("label" => "Combo Teste", "name" => "combo", "disabled" => false, "options" => array (array ("value" => "1", "selected" => 0, "text" => "Teste1"), array ("value" => "2", "selected" => 0, "text" => "Teste2"), array ("value" => "3", "selected" => 0, "text" => "Teste3")) );
     * @return string retorna uma string com o código HTML do ComboBox desejado.
     */

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * ComboBoxes.
     * @param array $dadosCombo
     * Recebe como parâmetro um array de dados contendo: Label, Nome, Disabled (true ou false), e um array 
     * interno para montagem das options contendo: Valor e Texto e se a opção 
     * deve vir selecionada ou não.
     * @example $dadoscombo =  array("label" => "Combo Teste", "name" => "combo", "disabled" => false, "options" => array (array ("value" => "1", "selected" => 0, "text" => "Teste1"), array ("value" => "2", "selected" => 0, "text" => "Teste2"), array ("value" => "3", "selected" => 0, "text" => "Teste3")) );
     * @param type $textoOpcaoPadrao Não obrigatório. Monta a primeira option 
     * com esse texto. Se não for informado monta com o padrão definido no 
     * cabeçalho. Se for informado null não monta essa primeira option.
     * @param type $onChange
     * @return string
     */
    function montaCombobox(array $dadosCombo, $textoOpcaoPadrao = 'Escolha uma op&ccedil&atilde;o abaixo...', $onChange = null/* EPC - 14/08/2016 - COMENTÁRIOS NO MÉTODO , $disabled = false */, $class = null) {
        $combobox = $this->montaComboboxParte1($dadosCombo, $textoOpcaoPadrao, $onChange/* EPC - 14/08/2016 - COMENTÁRIOS NO MÉTODO , $disabled */, $class);

        return $combobox;
    }

    function montaComboboxComCodigoExtra($codigoExtra, array $dadosCombo, $textoOpcaoPadrao = 'Escolha uma op&ccedil&atilde;o...', $onChange = null/* EPC - 14/08/2016 - COMENTÁRIOS NO MÉTODO , $disabled = false */) {
        $combobox = $this->montaComboboxParte1($dadosCombo, $textoOpcaoPadrao, $onChange/* EPC - 14/08/2016 - COMENTÁRIOS NO MÉTODO , $disabled */);
        $combobox .= $codigoExtra;

        return $combobox;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * Radio Buttons em posição de coluna.
     * @param array $dadosRadio
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das buttons contendo: Nome, Valor, Checked e Texto.
     * @example 
     * $dadosradio = array("label" => "*Sexo", "buttons" => array
     *     (array("name" => "sex", "value" => "masculino", "checked" => 1, "text" => "Masculino"), 
     *     array("name" => "sex", "value" => "feminino", "checked" => 0, "text" => "Feminino")));
     * @return string retorna uma string com o código HTML do Radio Button desejado.
     */
    function montaRadioEmColuna(array $dadosRadio, $class = null) {
        $radio = NULL;

        $radio .= "<label>{$dadosRadio['label']}</label>";

        foreach ($dadosRadio['buttons'] as $radioButton) {
            $checked = null;
            if ($radioButton['checked']) {
                //$checked = " checked='checked' ";
                $checked = " checked ";
            }

            $adicional = null;
            if (isset($radioButton["adicional"])) {
                $adicional = $radioButton["adicional"];
            }

            $radio .= "<input type='radio' name='{$radioButton['name']}' value='{$radioButton['value']}' class ='{$class}' {$checked} {$adicional} /> {$radioButton['text']}";
        }

        return $radio;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * Radio Buttons em posição de linha.
     * @param array $dadosRadio
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das buttons contendo: Nome, Valor, Checked, Texto e identificador.
     * @exemple
     * $dadosradio = array("label" => "*Sexo", "buttons" => array
     *     (array("name" => "sex", "value" => "masculino", "checked" => 1, "text" => "Masculino", id="versAtiva0"), 
     *     array("name" => "sex", "value" => "feminino", "checked" => 0, "text" => "Feminino", id="versAtiva1")));
     * @return string retorna uma string com o código HTML do Radio Button desejado.
     */
    function montaRadioEmLinha(array $dadosRadio) {
        $radio = NULL;
        $radio .= "<label class='rotulo-inline'>{$dadosRadio['label']}</label><br>";
        $primeiroRadio = TRUE;

        foreach ($dadosRadio['buttons'] as $dadosR) {
            $checked = null;
            if ($dadosR['checked']) {
                $checked = " checked ";
            }

            //cria e limpa uma variável para guardar o código com o identificador.
            $id = null;
            if (isset($dadosR['id'])) {
                if (is_null($dadosR['id'])) {
                    //se vazio não precisa fazer nada
                } else {
                    $id = "id=\"{$dadosR['id']}\" ";
                }
            }

//            if ($primeiroRadio) {
//                $primeiroRadio = FALSE;
            $radio .= "<label class='radio-inline'><input type='radio' {$id} name='{$dadosR['name']}' value='{$dadosR['value']}' {$checked}>{$dadosR['text']}</input></label>";
//            } else {
//                //$radio .= "<div><label><input type='radio' name='{$dadosR['name']}' value='{$dadosR['value']}' {$checked}></input>{$dadosR['text']}</label></div>";
//            }
        }

        return $radio;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de Radio Buttons em posição de um radio por linha.
     * 
     * @param array $dadosRadio
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das buttons contendo: Nome, Valor, Checked, Texto e identificador.
     * @exemple
     * $dadosradio = array("label" => "*Sexo", "buttons" => array
     *     (array("name" => "sex", "value" => "masculino", "checked" => 1, "text" => "Masculino", "id"="versAtiva0"), 
     *     array("name" => "sex", "value" => "feminino", "checked" => 0, "text" => "Feminino", "id"="versAtiva1")));
     * @return string retorna uma string com o código HTML do Radio Button desejado.
     */
    function montaRadioEmLinhas(array $dadosRadio) {
        $radio = NULL;
        $radio .= "<label>{$dadosRadio['label']}</label>";
        $primeiroRadio = TRUE;

        foreach ($dadosRadio['buttons'] as $dadosR) {
            $checked = null;
            if ($dadosR['checked']) {
                $checked = " checked ";
            }

            //verifica se o id foi informado e monta uma variável auxiliar
            $id = null;
            if (isset($dadosR["id"])) {
                $id = "id='{$dadosR['id']}'";
            }

            $input = "<input type='radio' name='{$dadosR['name']}' {$id} value='{$dadosR['value']}' {$checked}></input>{$dadosR['text']}";
            if ($primeiroRadio) {
                $primeiroRadio = FALSE;
                $radio .= "<label id='primeiroRadio'>{$input}</label><br>";
                //$radio .= "<label id='primeiroRadio'><input type='radio' name='{$dadosR['name']}' {$id} value='{$dadosR['value']}' {$checked}></input>{$dadosR['text']}</label></div><br>";
            } else {
                $radio .= "<label>{$input}</label><br>";
                //$radio .= "<label><input type='radio' name='{$dadosR['name']}' {$id} value='{$dadosR['value']}' {$checked}></input>{$dadosR['text']}</label><br>";
            }

            return $radio;
        }
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * Radio Buttons em posição de linha.
     * @param array $dadosRadio
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das buttons contendo: Nome, Valor, Checked e Texto.
     * @exemple
     * $dadosradio = array("label" => "*Sexo", "buttons" => array
     *     (array("name" => "sex", "value" => "masculino", "checked" => 1, "text" => "Masculino"), 
     *     array("name" => "sex", "value" => "feminino", "checked" => 0, "text" => "Feminino")));
     * @return string retorna uma string com o código HTML do Radio Button desejado.
     */
    function montaRadioEmLinha02(array $dadosRadio) {
        $radio = NULL;

        $radio .= "<label>{$dadosRadio['label']}</label>";


        foreach ($dadosRadio['buttons'] as $dadosR) {
            $checked = null;
            if ($dadosR['checked']) {
                $checked = " checked ";
            }

            $radio .= "<label><input type='radio' name='{$dadosR['name']}' value='{$dadosR['value']}' {$checked}></input>{$dadosR['text']}</label>";
        }


        return $radio;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * CheckBox em posição de linha.
      .     * @param array $dadosCheckbox
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das options contendo: Nome, Valor, Texto do checkbox, 
     * True/False para Checked e True/False para Disabled, onde esses dois últimos não são obrigatórios.
     * @example 
     * $dadisCheckbox = array("label" => "CheckBox Teste", "options' => array(array("name" => "teste1", "value" => "teste1", 
     * "text" => "Texto do checkbox", "checked" => true, "disabled" => false));
     * @return string retorna uma string com o código HTML do CheckBox desejado.
     */
    function montaCheckboxEmLinha(array $dadosCheckbox) {
        $disabled = null;

        if (is_null($dadosCheckbox['label'])) {
            $dadosCheckbox['label'] = '&nbsp';
        }

        $checkbox .= "<label>{$dadosCheckbox['label']}</label>";

        foreach ($dadosCheckbox['options'] as $dadosCheck) {
            $checked = $disabled = NULL;

            if (isset($dadosCheck['checked']) && $dadosCheck['checked']) {
                $checked = " checked ";
            }

            if (isset($dadosCheck['disabled']) && $dadosCheck['disabled']) {
                $disabled = " disabled ";
            }

            $checkbox .= "<label><input type='checkbox' name='{$dadosCheck['name']}' value='{$dadosCheck['value']}' $disabled  $checked>{$dadosCheck['text']}</label>";
        }

        return $checkbox;
    }

    function montaCheckboxEmLinhas(array $dadosCheckbox) {
        $checkbox = NULL;
        $checkbox .= "<label>{$dadosCheckbox['label']}</label>";
        $primeiraCheck = TRUE;


        foreach ($dadosCheckbox['options'] as $dadosCheck) {
            $checked = null;
            if ($dadosCheck['checked']) {
                $checked = 'checked';
            }

            if ($primeiraCheck) {
                $primeiraCheck = FALSE;
                $checkbox .= "<label id='primeiraCheck'><input type='checkbox' name='{$dadosCheck['name']}' value='{$dadosCheck['value']}' $checked></input>{$dadosCheck['text']}</label>";
            } else {
                $checkbox .= "<label><input type='checkbox' name='{$dadosCheck['name']}' value='{$dadosCheck['value']}' $checked></input>{$dadosCheck['text']}</label><br>";
            }
        }
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * CheckBox em posição de linha, trazendo o nome primeiro do que o CheckBox.
     * @param array $dadosCheckbox
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das options contendo: Nome, Valor, Texto do checkbox, 
     * True/False para Checked e True/False para Disabled, onde esses dois últimos não são obrigatórios.
     * @example 
     * $dadisCheckbox = array("label" => "CheckBox Teste", "options' => array(array("name" => "teste1", "value" => "teste1", 
     * "text" => "Texto do checkbox", "checked" => true, "disabled" => false));
     * @return string retorna uma string com o código HTML do CheckBox desejado.
     */
    function montaCheckboxEmLinhaComNomeEmPrimeiro(array $dadosCheckbox) {
        $disabled = null;

        if (is_null($dadosCheckbox['label'])) {
            $dadosCheckbox['label'] = '&nbsp';
        }

        $checkbox .= "<label>{$dadosCheckbox['label']}</label>";

        foreach ($dadosCheckbox['options'] as $dadosCheck) {
            $checked = $disabled = NULL;

            if (isset($dadosCheck['checked']) && $dadosCheck['checked']) {
                $checked = " checked ";
            }

            if (isset($dadosCheck['disabled']) && $dadosCheck['disabled']) {
                $disabled = " disabled ";
            }

            $checkbox .= "<label>{$dadosCheck['text']}<input type='checkbox' name='{$dadosCheck['name']}' value='{$dadosCheck['value']}' $disabled  $checked></label>";
        }

        return $checkbox;
    }

    /**
     * Este método tem como objetivo facilitar e padronizar a codificação de 
     * CheckBox em posição de coluna.
     * @param array $dadosCheckbox
     * Recebe como parâmetro um array de dados contendo: Label e um array 
     * interno para montagem das options contendo: Nome e Valor.
     * @example 
     * $dadoscheckbox = array("label" => "Opções", 
     *                        "options" => array(array("name" => "teste1", 
     *                                                 "value" => "teste1", 
     *                                                 "checked" => 1, 
     *                                                 "text" => "CheckBox Teste1"));
     * @return string retorna uma string com o código HTML do CheckBox desejado.
     */
    function montaCheckboxEmColuna(array $dadosCheckbox) {
        $checkbox = NULL;
        $checkbox .= "<label>{$dadosCheckbox['label']}</label>";

        foreach ($dadosCheckbox['options'] as $dadosCheck) {
            $checked = null;
            if ($dadosCheck['checked']) {
                $checked = 'checked';
            }

            $checkbox .= "<input type='checkbox' name='{$dadosCheck['name']}' value='{$dadosCheck['value']}' $checked>{$dadosCheck['text']}</input>";
        }

        return $checkbox;
    }

    /**
     * Este método monta uma tabela de acordo com os dados recebidos, isto é, ela é adaptável.
     * @param array $cabecalho Vetor com os títulos das colunas das tabelas
     * @param array $tamanhoDasColunas Vetor com os tamanhos das colunas
     * @param array $linhasEColunasArray Matriz com os dados. Cada linha deve ser 
     * um array com as colunas.
     * @param type $label Se informado identifica por meio de um label a tabela 
     * e envolve o label e a tabela com uma div. Não é obrigatório
     * @param string $classeCss Identifica o nome da classe css para a tabela. Se 
     * não for informada assume a classe default, ou seja 'tabela'.
     * @return string Código HTML da tabela
     * @example
     * $cabecalho      = array("Nome da coluna 1", "Nome da coluna 2");
      $linhasEColunas = array(
      array('Linha 1 / Coluna 1', 'Linha 1 / Coluna 2'),
      array('Linha 2 / Coluna 1', 'Linha 2 / Coluna 2')
      );
     */
    function montaTabelaInicio($label = null, $classesCss = 'tabela') {
        $tabela = null;

        if (is_null($label)) {
            // Sem Label. Não faz nada
        } else {
            // Caso um label tenha sido enviado, Cria uma <div> do tipo fieldset
            // envolvendo a tabela e cria uma label
            $tabela .= '<div class="linhaFieldset">';
            $tabela .= '<label>' . $label . '</label>';
        }

        // Classes CSS
        if (is_array($classesCss)) {
            $stringClassesCss = implode(' ', $classesCss);
        } else {
            $stringClassesCss = $classesCss;
        }

        // Inicia tabela
        $tabela .= '<div class="tabela-area">'
                . '<table class="' . $stringClassesCss . '" border = 3>';

        return $tabela;
    }

    function montaTabelaFim(array $tamanhoDasColunas, array $linhasEColunasArray, $label = null) {
        $tabela = null;
        // Monta o corpo
        if (is_array($linhasEColunasArray)) {
            $tabela .= '<tbody>';
            foreach ($linhasEColunasArray as $linhasEColunas) {
                if (is_array($linhasEColunas)) {
                    $tabela .= '<tr>';

                    $i = 0;
                    foreach ($linhasEColunas as $linhaEColuna) {
                        $tabela .= '<td width="' . $tamanhoDasColunas[$i] . '">' . $linhaEColuna . '</td>';
                        $i++;
                    }
                    $tabela .= '</tr>';
                }
            }
            $tabela .= '</tbody>';
        }

        $tabela .= '</table>';
        $tabela .= '</div>';

        if (is_null($label)) {
            // Não faz nada
        } else {
            $tabela .= '</div>';
        }

        $tabela .= '<div class="clearfix"></div>';

        return $tabela;
    }

    /**
     * Este método monta uma tabela de acordo com os dados recebidos, isto é, ela é adaptável.
     * @param array $cabecalho Vetor com os títulos das colunas das tabelas
     * @param array $tamanhoDasColunas Vetor com os tamanhos das colunas
     * @param array $linhasEColunasArray Matriz com os dados. Cada linha deve ser 
     * um array com as colunas.
     * @param type $label Se informado identifica por meio de um label a tabela 
     * e envolve o label e a tabela com uma div. Não é obrigatório
     * @param string $classeCss Identifica o nome da classe css para a tabela. Se 
     * não for informada assume a classe default, ou seja 'tabela'.
     * @return string Código HTML da tabela
     * @example
     * $cabecalho      = array("Nome da coluna 1", "Nome da coluna 2");
      $linhasEColunas = array(
      array('Linha 1 / Coluna 1', 'Linha 1 / Coluna 2'),
      array('Linha 2 / Coluna 1', 'Linha 2 / Coluna 2')
      );
     */
    function montaTabela(array $cabecalho, array $tamanhoDasColunas, array $linhasEColunasArray, $label = null, $classeCss = 'table') {
        $tabela = null;
        $tabela .= $this->montaTabelaInicio($label, $classeCss);

        if (is_null($cabecalho)) {
            // faz nada
        } else {
            // Monta o cabeçalho
            if (is_array($cabecalho)) {
                $tabela .= '<thead>';
                $tabela .= '<tr>';
                foreach ($cabecalho as $nome) {
                    $tabela .= '<th>' . $nome . '</th>';
                }
                $tabela .= '</tr>';
                $tabela .= '</thead>';
            }
        }

        $tabela .= $this->montaTabelaFim($tamanhoDasColunas, $linhasEColunasArray, $label);
        return $tabela;
    }

    function montaTabelaCentralizada(array $cabecalho, array $tamanhoDasColunas, array $linhasEColunasArray, $label = null, $classesCss = 'tabela-centralizada') {
        return $this->montaTabela($cabecalho, $tamanhoDasColunas, $linhasEColunasArray, $label, $classesCss);
    }

    public function montaTabelaSemCabecalhoSemBorda(array $tamanhoDasColunas, array $linhasEColunasArray, $label = null, $classesCss = 'removeBordaDaTabela') {
        return $this->montaTabelaSemCabecalho($tamanhoDasColunas, $linhasEColunasArray, $label, $classesCss);
    }

    function montaTabelaCentralizadaSemCabecalho(array $tamanhoDasColunas, array $linhasEColunasArray, $label = null, $classesCss = 'tabela-centralizada') {
        return $this->montaTabelaSemCabecalho($tamanhoDasColunas, $linhasEColunasArray, $label, $classesCss);
    }

    /**
     * Este método monta uma tabela de acordo com os dados recebidos, isto é, ela é adaptável.
     * @param array $cabecalho Vetor com os títulos das colunas das tabelas
     * @param array $tamanhoDasColunas Vetor com os tamanhos das colunas
     * @param array $linhasEColunasArray Matriz com os dados. Cada linha deve ser 
     * um array com as colunas.
     * @param type $label Se informado identifica por meio de um label a tabela 
     * e envolve o label e a tabela com uma div. Não é obrigatório
     * @param string $classesCss Identifica o nome classe css para a tabela. Se 
     * não for informada assume a classe default 'tabela'
     * @return string Código HTML da tabela
     * @example
     * $cabecalho      = array("Nome da coluna 1", "Nome da coluna 2");
      $linhasEColunas = array(
      array('Linha 1 / Coluna 1', 'Linha 1 / Coluna 2'),
      array('Linha 2 / Coluna 1', 'Linha 2 / Coluna 2')
      );
     */
    function montaTabelaSemCabecalho(array $tamanhoDasColunas, array $linhasEColunasArray, $label = null, $classesCss = 'tabela') {
        $tabela = null;
        $tabela .= $this->montaTabelaInicio($label = null, $classesCss);
        $tabela .= $this->montaTabelaFim($tamanhoDasColunas, $linhasEColunasArray, $label = null);
        return $tabela;
    }

    /**
     * Este método monta o combobox pronto com todos os estados brasileiros
     * @param array $dadosCombo
     * @param string $selecionado
     * @param string $textoOpcaoPadrao
     * @param string $onChange
     * @param boolean $disabled
     * Recebe como parâmetro um array de dados contendo: Label e Nome do campo
     * para montagem das options contendo: Valor e Texto e se a opção deve vir selecionada ou não.
     * @example $dadoscombo = array("label" => "Combo Teste", "name" => "combo");
     * @return string retorna uma string com o código HTML do ComboBox desejado.
     */
    function montaComboboxComEstadosBrasileiros(array $dadosCombo, $selecionado = null, $textoOpcaoPadrao = 'Escolha um estado...', $onChange = null, $disabled = false) {
        $estados = array();
        $estados[] = "AC";
        $estados[] = "AL";
        $estados[] = "AM";
        $estados[] = "AP";
        $estados[] = "BA";
        $estados[] = "CE";
        $estados[] = "DF";
        $estados[] = "ES";
        $estados[] = "GO";
        $estados[] = "MA";
        $estados[] = "MG";
        $estados[] = "MS";
        $estados[] = "MT";
        $estados[] = "PA";
        $estados[] = "PB";
        $estados[] = "PE";
        $estados[] = "PI";
        $estados[] = "PR";
        $estados[] = "RJ";
        $estados[] = "RN";
        $estados[] = "RO";
        $estados[] = "RR";
        $estados[] = "RS";
        $estados[] = "SC";
        $estados[] = "SE";
        $estados[] = "SP";
        $estados[] = "TO";

        $montaArrayEstadosCBB = array();
        foreach ($estados as $estado) {
            $selected = 0;
            if ($estado == $selecionado) {
                $selected = 1;
            }
            $montaArrayEstadosCBB[] = array(
                "value" => $estado,
                "selected" => $selected,
                "disabled" => $disabled,
                "text" => $estado);
        }

        $dadosCombo["options"] = $montaArrayEstadosCBB;

        return $this->montaCombobox($dadosCombo, $textoOpcaoPadrao, $onChange/* , $disabled */);
    }

}
