<?php

require_once '../Views/TreinadorView.php';
require_once '../Models/TreinadorModel.php';
require_once '../ADOs/TreinadorAdo.php';
require_once 'ControllerAbstract.php';

class TreinadorController extends ControllerAbstract {

    private $treinadorView = null;
    private $treinadorModel = null;
    private $treinadorAdo = null;
    private $acao = null;

    public function __construct() {
        $this->treinadorAdo = new TreinadorAdo();
        $this->treinadorModel = new TreinadorModel();
        $this->treinadorView = new TreinadorView("Cadastro de Treinadores");

        $this->acao = $this->treinadorView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }

        $this->treinadorView->displayInterface($this->treinadorModel);
    }

    public function cadastraObjeto() {
        $this->treinadorModel = $this->treinadorView->recebeDados();
        
        $this->treinadorModel->setTreiId(null);

        if ($this->treinadorModel->checaAtributos($this->treinadorView)) {
            $cadastrou = $this->treinadorAdo->insereObjeto($this->treinadorModel);

            if ($cadastrou) {
                $this->treinadorView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->treinadorView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        $treinadorModel = $this->treinadorView->recebeDadosDaConsulta();

        $buscou = $this->treinadorModel = $this->treinadorAdo->buscaTreinador($treinadorModel->getTreiId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->treinadorView->adicionaMensagensDeErro("Não foi possível encontrar o Treinador selecionado!");
            } else {
                $this->treinadorView->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->treinadorModel = new TreinadorModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->treinadorModel = $this->treinadorView->recebeDados();

        if ($this->treinadorModel->checaAtributos($this->treinadorView)) {
            $alterou = $this->treinadorAdo->alteraObjeto($this->treinadorModel);

            if ($alterou) {
                $this->treinadorView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->treinadorView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $this->treinadorModel = $this->treinadorView->recebeDados();

        $excluiu = $this->treinadorAdo->excluiObjeto($this->treinadorModel);

        if ($excluiu) {
            $this->treinadorView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
            $this->treinadorModel = new TreinadorModel();
        } else {
            $this->treinadorView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }
    }

    public function limpaObjeto() {
        $this->treinadorModel = new TreinadorModel();
    }

}
