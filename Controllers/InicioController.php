<?php

require_once '../Views/InicioView.php';
require_once 'ControllerAbstract.php';

class InicioController extends ControllerAbstract {

    private $inicioView = null;

    public function __construct() {
        $this->inicioView = new InicioView("Início");

        $this->inicioView->displayInterface(null);
    }

    public function cadastraObjeto() {
        
    }

    public function consultaObjeto() {
        
    }

    public function alteraObjeto() {
        
    }

    public function excluiObjeto() {
        
    }

    public function limpaObjeto() {
        
    }

}
