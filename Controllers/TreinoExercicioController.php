<?php

require_once '../Views/TreinoExercicioView.php';
require_once '../Models/TreinoExercicioModel.php';
require_once '../ADOs/TreinoExercicioAdo.php';
require_once 'ControllerAbstract.php';

class TreinoExercicioController extends ControllerAbstract {

    private $treinoExercicioView = null;
    private $treinoExercicioModel = null;
    private $treinoExercicioAdo = null;
    private $acao = null;
    private $trenId = null;

    public function __construct() {
        $this->treinoExercicioAdo = new TreinoExercicioAdo();
        $this->treinoExercicioModel = new TreinoExercicioModel();
        $this->treinoExercicioView = new TreinoExercicioView("Cadastro de Exercícios do Treino");

        $this->acao = $this->treinoExercicioView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }

        $this->treinoExercicioView->displayInterface($this->treinoExercicioModel);
    }

    public function cadastraObjeto() {
        $this->treinoExercicioModel = $this->treinoExercicioView->recebeDados();
        
        $this->treinoExercicioModel->setTrexId(null);

        if ($this->treinoExercicioModel->checaAtributos($this->treinoExercicioView)) {
            $cadastrou = $this->treinoExercicioAdo->insereObjeto($this->treinoExercicioModel);

            if ($cadastrou) {
                $this->treinoExercicioView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->treinoExercicioView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        
        $this->treinoExercicioModel = $this->treinoExercicioView->recebeDadosDaConsulta();

        if ($this->trenId != NULL) {
            $this->treinoExercicioModel->setTrenId($this->trenId);
        }

        $buscou = $this->treinoExercicioModel = $this->treinoExercicioAdo->buscaTreinoExercicio($this->treinoExercicioModel->getTrenId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->treinoExercicioView->adicionaMensagensDeErro("Não foi possível encontrar os Exercícios para o Treino selecionado!");
            } else {
                $this->treinoExercicioView->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->treinoExercicioModel = new TreinoExercicioModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->treinoExercicioModel = $this->treinoExercicioView->recebeDados();

        if ($this->treinoExercicioModel->checaAtributos($this->treinoExercicioView)) {
            $alterou = $this->treinoExercicioAdo->alteraObjeto($this->treinoExercicioModel);

            if ($alterou) {
                $this->treinoExercicioView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->treinoExercicioView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $trexTbId = $_POST['trexTbId'];
        $this->treinoExercicioModel = $this->treinoExercicioAdo->buscaObjetoComPs(array($trexTbId), $where = 'trex_id = ?');
        $this->trenId = $this->treinoExercicioModel->getTrenId();
        $_POST['idConsulta'] = $this->trenId;
        
        $excluiu = $this->treinoExercicioAdo->excluiExercicioTreino($trexTbId);

        if ($excluiu) {
            $this->treinoExercicioView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
        } else {
            $this->treinoExercicioView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }

        $this->consultaObjeto();
    }

    public function limpaObjeto() {
        $this->treinoExercicioModel = new TreinoExercicioModel();
    }

}
