<?php

abstract class ControllerAbstract {

    abstract function consultaObjeto();

    abstract function cadastraObjeto();

    abstract function alteraObjeto();

    abstract function excluiObjeto();

    abstract function limpaObjeto();
}
