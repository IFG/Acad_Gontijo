<?php

require_once '../Views/TreinoView.php';
require_once '../Models/TreinoModel.php';
require_once '../ADOs/TreinoAdo.php';
require_once 'ControllerAbstract.php';

class TreinoController extends ControllerAbstract {

    private $treinoView = null;
    private $treinoModel = null;
    private $treinoAdo = null;
    private $acao = null;

    public function __construct() {
        $this->treinoAdo = new TreinoAdo();
        $this->treinoModel = new TreinoModel();
        $this->treinoView = new TreinoView("Cadastro de Treinos");

        $this->acao = $this->treinoView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }

        $this->treinoView->displayInterface($this->treinoModel);
    }

    public function cadastraObjeto() {
        $this->treinoModel = $this->treinoView->recebeDados();
        
        $this->treinoModel->setTrenId(null);
        
        if ($this->treinoModel->checaAtributos($this->treinoView)) {
            $cadastrou = $this->treinoAdo->insereObjeto($this->treinoModel);

            if ($cadastrou) {
                $this->treinoView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->treinoView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        $this->treinoModel = $this->treinoView->recebeDadosDaConsulta();

        $buscou = $this->treinoModel = $this->treinoAdo->buscaTreino($this->treinoModel->getTrenId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->treinoView->adicionaMensagensDeErro("Não foi possível encontrar o Treino selecionado!");
            } else {
                $this->treinoView->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->treinoModel = new TreinoModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->treinoModel = $this->treinoView->recebeDados();

        if ($this->treinoModel->checaAtributos($this->treinoView)) {
            $alterou = $this->treinoAdo->alteraObjeto($this->treinoModel);

            if ($alterou) {
                $this->treinoView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->treinoView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $this->treinoModel = $this->treinoView->recebeDados();

        $excluiu = $this->treinoAdo->excluiObjeto($this->treinoModel);

        if ($excluiu) {
            $this->treinoView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
            $this->treinoModel = new TreinoModel();
        } else {
            $this->treinoView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }
    }

    public function limpaObjeto() {
        $this->treinoModel = new TreinoModel();
    }

}
