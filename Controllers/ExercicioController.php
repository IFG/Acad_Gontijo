<?php

require_once '../Views/ViewAbstract.php';
require_once '../Views/ExercicioView.php';
require_once '../Models/ExercicioModel.php';
require_once '../ADOs/ExercicioAdo.php';
require_once 'ControllerAbstract.php';

class ExercicioController extends ControllerAbstract {

    private $exercicioView = null;
    private $exercicioModel = null;
    private $exercicioAdo = null;
    private $acao = null;

    public function __construct() {
        $this->exercicioAdo = new ExercicioAdo();
        $this->exercicioModel = new ExercicioModel();
        $this->exercicioView = new ExercicioView("Cadastro de Exercícios");

        $this->acao = $this->exercicioView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }
        
        $this->exercicioView->displayInterface($this->exercicioModel);
    }

    public function cadastraObjeto() {
        $this->exercicioModel = $this->exercicioView->recebeDados();
        
        $this->exercicioModel->setExerId(null);

        if ($this->exercicioModel->checaAtributos($this->exercicioView)) {
            $cadastrou = $this->exercicioAdo->insereObjeto($this->exercicioModel);

            if ($cadastrou) {
                $this->exercicioView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->exercicioView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        $exercicioModel = $this->exercicioView->recebeDadosDaConsulta();

        $buscou = $this->exercicioModel = $this->exercicioAdo->buscaExercicio($exercicioModel->getExerId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->exercicioView->adicionaMensagensDeErro("Não foi possível encontrar o Exercicio selecionado!");
            } else {
                $this->exercicioView->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->exercicioModel = new ExercicioModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->exercicioModel = $this->exercicioView->recebeDados();

        if ($this->exercicioModel->checaAtributos($this->exercicioView)) {
            $alterou = $this->exercicioAdo->alteraObjeto($this->exercicioModel);

            if ($alterou) {
                $this->exercicioView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->exercicioView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $this->exercicioModel = $this->exercicioView->recebeDados();

        $excluiu = $this->exercicioAdo->excluiObjeto($this->exercicioModel);

        if ($excluiu) {
            $this->exercicioView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
            $this->exercicioModel = new ExercicioModel();
        } else {
            $this->exercicioView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }
    }

    public function limpaObjeto() {
        $this->exercicioModel = new ExercicioModel();
    }

}
