<?php

require_once '../Views/ViewAbstract.php';
require_once '../Views/TipoDeTreinoView.php';
require_once '../Models/TipoDeTreinoModel.php';
require_once '../ADOs/TipoDeTreinoAdo.php';
require_once 'ControllerAbstract.php';

class TipoDeTreinoController extends ControllerAbstract {

    private $tipoDeTreinoView = null;
    private $tipoDeTreinoModel = null;
    private $tipoDeTreinoAdo = null;
    private $acao = null;

    public function __construct() {
        $this->tipoDeTreinoAdo = new TipoDeTreinoAdo();
        $this->tipoDeTreinoModel = new TipoDeTreinoModel();
        $this->tipoDeTreinoView = new TipoDeTreinoView("Cadastro de Tipos de Treino");

        $this->acao = $this->tipoDeTreinoView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }

        $this->tipoDeTreinoView->displayInterface($this->tipoDeTreinoModel);
    }

    public function cadastraObjeto() {
        $this->tipoDeTreinoModel = $this->tipoDeTreinoView->recebeDados();
        
        $this->tipoDeTreinoModel->setTptrId(null);

        if ($this->tipoDeTreinoModel->checaAtributos($this->tipoDeTreinoView)) {
            $cadastrou = $this->tipoDeTreinoAdo->insereObjeto($this->tipoDeTreinoModel);

            if ($cadastrou) {
                $this->tipoDeTreinoView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->tipoDeTreinoView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        $tipoDeTreinoModel = $this->tipoDeTreinoView->recebeDadosDaConsulta();

        $buscou = $this->tipoDeTreinoModel = $this->tipoDeTreinoAdo->buscaTipoDeTreino($tipoDeTreinoModel->getTptrId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->tipoDeTreinoView->adicionaMensagensDeErro("Não foi possível encontrar o Tipo de Treino selecionado!");
            } else {
                $this->tipoDeTreinoVew->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->tipoDeTreinoModel = new TipoDeTreinoModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->tipoDeTreinoModel = $this->tipoDeTreinoView->recebeDados();

        if ($this->tipoDeTreinoModel->checaAtributos($this->tipoDeTreinoView)) {
            $alterou = $this->tipoDeTreinoAdo->alteraObjeto($this->tipoDeTreinoModel);

            if ($alterou) {
                $this->tipoDeTreinoView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->tipoDeTreinoView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $this->tipoDeTreinoModel = $this->tipoDeTreinoView->recebeDados();
        
        $excluiu = $this->tipoDeTreinoAdo->excluiObjeto($this->tipoDeTreinoModel);

        if ($excluiu) {
            $this->tipoDeTreinoView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
            $this->tipoDeTreinoModel = new TipoDeTreinoModel();
        } else {
            $this->tipoDeTreinoView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }
    }

    public function limpaObjeto() {
        $this->tipoDeTreinoModel = new TipoDeTreinoModel();
    }

}
