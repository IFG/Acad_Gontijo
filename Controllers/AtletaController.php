<?php

require_once '../Views/ViewAbstract.php';
require_once '../Views/AtletaView.php';
require_once '../Models/AtletaModel.php';
require_once '../ADOs/AtletaAdo.php';
require_once 'ControllerAbstract.php';

class AtletaController extends ControllerAbstract {

    private $atletaView = null;
    private $atletaModel = null;
    private $atletaAdo = null;
    private $acao = null;

    public function __construct() {
        $this->atletaAdo = new AtletaAdo();
        $this->atletaModel = new AtletaModel();
        $this->atletaView = new AtletaView("Cadastro de Atletas");

        $this->acao = $this->atletaView->getAcao();

        switch ($this->acao) {
            case 'cad':
                $this->cadastraObjeto();
                break;

            case 'con':
                $this->consultaObjeto();
                break;

            case 'alt':
                $this->alteraObjeto();
                break;

            case 'exc':
                $this->excluiObjeto();
                break;

            case 'lim':
                $this->limpaObjeto();
                break;

            default:
                break;
        }

        $this->atletaView->displayInterface($this->atletaModel);
    }

    public function cadastraObjeto() {
        $this->atletaModel = $this->atletaView->recebeDados();
        
        $this->atletaModel->setAtleId(null);

        if ($this->atletaModel->checaAtributos($this->atletaView)) {
            $cadastrou = $this->atletaAdo->insereObjeto($this->atletaModel);
            if ($cadastrou) {
                $this->atletaView->adicionaMensagensDeSucesso("Cadastro efetuado com sucesso!");
            } else {
                $this->atletaView->adicionaMensagensDeErro("Cadastro não foi efetuado com sucesso!");
            }
        }
    }

    public function consultaObjeto() {
        $atletaModel = $this->atletaView->recebeDadosDaConsulta();

        $buscou = $this->atletaModel = $this->atletaAdo->buscaAtleta($atletaModel->getAtleId());

        if (!$buscou) {
            if ($buscou === 0) {
                $this->atletaView->adicionaMensagensDeErro("Não foi possível encontrar o Atleta selecionado!");
            } else {
                $this->atletaView->adicionaMensagensDeErro("Ocorreu um erro na consulta! Contate o analista responsável.");
            }

            $this->atletaModel = new atletaModel();
            return;
        }
    }

    public function alteraObjeto() {
        $this->atletaModel = $this->atletaView->recebeDados();

        if ($this->atletaModel->checaAtributos($this->atletaView)) {
            $alterou = $this->atletaAdo->alteraObjeto($this->atletaModel);

            if ($alterou) {
                $this->atletaView->adicionaMensagensDeSucesso("Alteração efetuada com sucesso!");
            } else {
                $this->atletaView->adicionaMensagensDeErro("Alteração não foi efetuada com sucesso!");
            }
        }
    }

    public function excluiObjeto() {
        $this->atletaModel = $this->atletaView->recebeDados();

        $excluiu = $this->atletaAdo->excluiObjeto($this->atletaModel);

        if ($excluiu) {
            $this->atletaView->adicionaMensagensDeSucesso("Exclusão efetuada com sucesso!");
            $this->atletaModel = new AtletaModel();
        } else {
            $this->atletaView->adicionaMensagensDeErro("Exclusão não foi efetuada com sucesso!");
        }
    }

    public function limpaObjeto() {
        $this->atletaModel = new AtletaModel();
    }

}
