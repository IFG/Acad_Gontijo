<?php

require_once '../Views/LoginView.php';
require_once '../Views/InicioView.php';
require_once '../ADOs/UsuarioAdo.php';
require_once 'ControllerAbstract.php';

class LoginController extends ControllerAbstract {

    private $loginView = null;
    private $usuarioAdo = null;
    private $usuarioModel = null;
    private $acao = null;
     
    public function __construct() {
        $this->loginView = new LoginView("Login");
        $this->usuarioAdo = new UsuarioAdo();
        $this->usuarioModel = new UsuarioModel;
        
        $this->acao = $this->loginView->getAcao();
        
        switch ($this->acao){
            case 'log':
                $this->loga();
                break;
        }
        $this->loginView->displayInterface(null);
    }

    public function cadastraObjeto() {
        
    }

    public function consultaObjeto() {
        
    }

    public function alteraObjeto() {
        
    }

    public function excluiObjeto() {
        
    }

    public function limpaObjeto() {
        
    }
    
    public function loga(){
        $erro = FALSE;
        
        $this->usuarioModel = $this->loginView->recebeDados();
        $logou = $this->usuarioModel = $this->usuarioAdo->buscaUsuarioExistente($this->usuarioModel->getUsuaLogin(),$this->usuarioModel->getUsuaSenha());
        if($logou){
            //continua
        }else{
            $erro = TRUE;
            $this->loginView->adicionaMensagensDeErro("Login ou senha errado, insira corretamente.");
        }
        
        if($erro){
            //caso erro não cria sessão, encerra por aki.
            return;
        }
        
        session_start();
        
        $_SESSION['nome'] =  $this->usuarioModel->getUsuaNome();
        $_SESSION['login'] = $this->usuarioModel->getUsuaLogin();
        $_SESSION['senha'] = $this->usuarioModel->getUsuaSenha();
        $_SESSION['nivel'] = $this->usuarioModel->getUsuaNivel();
        
        header('Location: ' . "InicioModulo.php");
    }

}
