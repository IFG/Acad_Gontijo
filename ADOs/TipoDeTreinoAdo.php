<?php

require_once '../BD/AdoPdoAbstract.php';

class TipoDeTreinoAdo extends AdoPdoAbstract {

    public function __construct() {
        parent::__construct();
        parent::setNomeDaTabela("Tipos_de_Treinos");
    }

    public function insereObjeto(ModelAbstract $tipoDeTreinoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($tipoDeTreinoModel);
        $query = parent::montaInsertDoObjetoPS(parent::getNomeDaTabela(), $colunasValores);
        return parent::executaPs($query, $colunasValores);
    }

    public function alteraObjeto(ModelAbstract $tipoDeTreinoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($tipoDeTreinoModel);
        $where = "tptr_id = ?";
        $query = parent::montaUpdateDoObjetoPS(parent::getNomeDaTabela(), $colunasValores, $where);

        $colunasValores ['tptr_id_where'] = $tipoDeTreinoModel->getTptrId();
        return parent::executaPs($query, $colunasValores);
    }

    public function excluiObjeto(ModelAbstract $tipoDeTreinoModel) {
        $where = "tptr_id = ?";
        $query = parent::montaDeleteDoObjeto(parent::getNomeDaTabela(), $where);
        
        return parent::executaPs($query, array($tipoDeTreinoModel->getTptrId()));
    }
    
    public function buscaTipoDeTreino($tptrId) {
        $where = " tptr_id = ? ";
        return parent::buscaObjetoComPs(array($tptrId), $where);
    }
    
    public function buscaNomes($q) {
        $query = "select tptr_nome from Tipos_de_Treinos where tptr_nome LIKE '%?%'";

        $consultou = parent::executaPs($query, array($q));
        if ($consultou) {
            //continua... 
        } else {
            if (parent::qtdeLinhas() === 0) {
                return 0;
            }
            return FALSE;
        }

        $nomes = array();

        while ($tupla = parent::leTabelaBD()) {
            $nomes [] = $tupla ['tptr_nome'];
        }

        return $nomes;
    }
}
