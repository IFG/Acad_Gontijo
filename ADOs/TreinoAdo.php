<?php

require_once '../BD/AdoPdoAbstract.php';

class TreinoAdo extends AdoPdoAbstract {

    public function __construct() {
        parent::__construct();
        parent::setNomeDaTabela("Treinos");
    }

    public function insereObjeto(ModelAbstract $treinoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($treinoModel);
        $query = parent::montaInsertDoObjetoPS(parent::getNomeDaTabela(), $colunasValores);

        return parent::executaPs($query, $colunasValores);
    }

    public function alteraObjeto(ModelAbstract $treinoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($treinoModel);
        $where = " tren_id = ? ";
        $query = parent::montaUpdateDoObjetoPS(parent::getNomeDaTabela(), $colunasValores, $where);

        //Acrescento mais uma posição no array para o ? do where. Tem que ser 
        //depois do montaUpdate pq ele não pode ser incluido na instrução update.
        $colunasValores ['tren_id_where'] = $treinoModel->getTrenId();

        return parent::executaPs($query, $colunasValores);
    }

    public function excluiObjeto(ModelAbstract $treinoModel) {
        $where = " tren_id = ? ";
        $query = parent::montaDeleteDoObjeto(parent::getNomeDaTabela(), $where);

        return parent::executaPs($query, array($treinoModel->getTrenId()));
    }

    public function buscaTreino($trenId) {
        $where = " tren_id = ? ";
        return parent::buscaObjetoComPs(array($trenId), $where);
    }

    public function buscaNomes($q) {
        
    }

}
