<?php

require_once '../BD/AdoPdoAbstract.php';

class UsuarioAdo extends AdoPdoAbstract {

    public function __construct() {
        parent::__construct();
        parent::setNomeDaTabela("Usuarios");
    }

    public function alteraObjeto(\ModelAbstract $objetoModel) {
        
    }

    public function excluiObjeto(\ModelAbstract $objetoModel) {
        
    }

    public function insereObjeto(\ModelAbstract $objetoModel) {
        
    }

    public function buscaUsuarioExistente($login,$senha) {
        return parent::buscaObjetoComPs(array($login,$senha),$where = "usua_login = ? AND usua_senha = ?");
    }
}