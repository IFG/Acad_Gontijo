<?php

require_once '../BD/AdoPdoAbstract.php';
require_once '../Models/ExercicioModel.php';

class TreinoExercicioAdo extends AdoPdoAbstract {

    public function __construct() {
        parent::__construct();
        parent::setNomeDaTabela("Treinos_Exercicios");
    }

    public function insereObjeto(ModelAbstract $treinamentoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($treinamentoModel);
        $query = parent::montaInsertDoObjetoPS(parent::getNomeDaTabela(), $colunasValores);

        return parent::executaPs($query, $colunasValores);
    }

    public function alteraObjeto(ModelAbstract $treinamentoModel) {
        $colunasValores = parent::montaArrayDeDadosDaTabela($treinamentoModel);
        $where = " tret_id = ? ";
        $query = parent::montaUpdateDoObjetoPS(parent::getNomeDaTabela(), $colunasValores, $where);

        //Acrescento mais uma posição no array para o ? do where. Tem que ser 
        //depois do montaUpdate pq ele não pode ser incluido na instrução update.
        $colunasValores ['tret_id_where'] = $treinamentoModel->getTretId();

        return parent::executaPs($query, $colunasValores);
    }

    public function excluiObjeto(ModelAbstract $treinoExercicioModel) {
        $where = " trex_id = ? ";
        $query = parent::montaDeleteDoObjeto(parent::getNomeDaTabela(), $where);

        return parent::executaPs($query, array($treinoExercicioModel->getTrexId()));
    }

    public function excluiExercicioTreino($trexTbId) {
        $where = " trex_id = ? ";
        $query = parent::montaDeleteDoObjeto(parent::getNomeDaTabela(), $where);

        return parent::executaPs($query, array($trexTbId));
    }

    public function buscaTreinoExercicio($trenId) {
        $where = "tren_id = ?";
        return parent::buscaObjetoComPs(array($trenId), $where);
    }

    public function buscaExerciciosPorTreino($trenId) {
        
        $exercicioModel = null;
        $exerciciosModel = null;

        $query = "SELECT exer_id, exer_nome, exer_descricao, tptr_nome, tren_id"
                . " FROM Exercicios INNER JOIN Treinos"
                . " ON Exercicios.tptr_id = Treinos.tptr_id"
                . " INNER JOIN Tipos_De_Treinos"
                . " ON Exercicios.tptr_id = Tipos_De_Treinos.tptr_id"
                . " WHERE Treinos.tren_id = ?";

        $resultado = parent::executaPs($query, array($trenId));

        if ($resultado) {
            //consulta Ok. Continua.
        } else {
            parent::setMensagem("Erro no select de buscaExerciciosPorTreino: " . parent::getBdError());
            return false;
        }

        while ($exercicio = parent::leTabelaBD()) {
            $exercicioModel = new stdClass();
            $exercicioModel->exer_id = $exercicio['exer_id'];
            $exercicioModel->exer_nome = $exercicio['exer_nome'];
            $exercicioModel->exer_descricao = $exercicio['exer_descricao'];
            $exercicioModel->tptr_nome = $exercicio['tptr_nome'];
            $exercicioModel->tren_id = $exercicio['tren_id'];
            $exerciciosModel[] = $exercicioModel;
        }

        return $exerciciosModel;
    }

}
