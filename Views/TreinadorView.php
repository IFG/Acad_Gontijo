<?php

require_once 'ViewAbstract.php';
require_once '../ADOs/TreinadorAdo.php';
require_once '../Models/TreinadorModel.php';
require_once '../Classes/MontaHtml.php';

class TreinadorView extends ViewAbstract {

    private function montaOptionsDaConsultaDeTreinadores($treiId) {
        $treinadoresAdo = new TreinadorAdo();
        $optionsTreinadores = null;
        $buscou = $treinadorModel = $treinadoresAdo->buscaArrayObjetoComPs(array(), 1, "order by trei_id");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Treinador!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Treinador! Contate o analista responsável pelo sistema.");
            }
            $treinadorModel = array();
        }

        foreach ($treinadorModel as $treinadorModel) {
            $selected = null;

            if ($treinadorModel->getTreiId() == $treiId) {
                $selected = 1;
            }

            $text = "Nome: " . $treinadorModel->getTreiNome();
            $optionsTreinadores[] = array("value" => $treinadorModel->getTreiId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTreinadores;
    }

    protected function montaHtmlConsulta($treinadorModel) {
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;

        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboTreinadores = array("label" => "Treinadores", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeTreinadores($treinadorModel->getTreiId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboTreinadores, $textoPadrao = 'Escolha um Treinador...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Treinador Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div>";

        $htmlConsulta .= "</form> "
                . "</fieldset>";

        return $htmlConsulta;
    }

    protected function montaCorpo($treinadorModel) {
        $titulo = "<h3>Cadastro de Treinadores</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($treinadorModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($treinadorModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($treinoModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados do Treinador</legend>";

        $dadosFieldsetHidden = array("name" => "treiId", "value" => $treinoModel->getTreiId());

        $htmlFieldsetNome = array("label" => "Nome", "type" => "text", "name" => "treiNome", "value" => $treinoModel->getTreiNome(), "class" => "form-control");

        $m = $f = 0;
        if ($treinoModel->getTreiSexo() == "F") {
            $f = 1;
        } else {
            $m = 1;
        }

        $htmRadioSexo = array("label" => "Sexo", "buttons" => array
                (array("name" => "treiSexo", "value" => "M", "checked" => $m, "text" => "Masculino"),
                array("name" => "treiSexo", "value" => "F", "checked" => $f, "text" => "Feminino")));

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetNome);
        $htmlDados .= "</div><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaRadioEmLinha($htmRadioSexo);
        $htmlDados .= "</div>";

        $disabled_cad_lim = null;
        if ($treinoModel->getTreiId() != NULL) {
            $disabled_cad_lim = "disabled";
        }

        $disabled_alt_exc = null;
        if ($treinoModel->getTreiId() == NULL) {
            $disabled_alt_exc = "disabled";
        }

        $htmlDados .= "<div class='col-xs-12'></br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' {$disabled_cad_lim} title='Clique para Cadastrar os Dados do Treinador.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            <button name='acao' class='btn btn-warning' type='submit' value='alt' {$disabled_alt_exc} title='Clique para Alterar os Dados do Treinador, Disponível apenas após a Consulta.'><i class='fa fa-pencil-square' aria-hidden='true'></i> Alterar</button>
            <button name='acao' class='btn btn-danger' type='submit' value='exc' {$disabled_alt_exc} title='Clique para Excluir os Dados do Treinador, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button>
            <button name='acao' class='btn btn-primary' type='submit' value='lim' title='Clique para Limpar Todos os Campos.'><i class='fa fa-refresh' aria-hidden='true'></i> Limpar</button>
            </form></fieldset>";

        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {
        $treinadorModel = new TreinadorModel();

        $treinadorModel->setTreiId($_POST['idConsulta']);

        return $treinadorModel;
    }

    public function recebeDados() {
        $treinadorModel = new TreinadorModel();

        $treinadorModel->setTreiId($_POST['treiId']);
        $treinadorModel->setTreiNome($_POST['treiNome']);
        $treinadorModel->setTreiSexo($_POST['treiSexo']);

        return $treinadorModel;
    }

}
