<?php

require_once 'ViewAbstract.php';

class InicioView extends ViewAbstract {

    protected function montaCorpo($inicio = null) {
        // Verifica se não há a variável da sessão que identifica o usuário
        if (isset($_SESSION['nome'])) {
            $titulo = "<center><h1><b>Bem-Vindo a Academia Gontijo</b></h1><center>";
            $observacao = "<center><h5><b>*Para executar qualquer ação no sistema, acesse o Menu no lado superior esquerdo! </b></h5><center>";
            //$img = "<img src='../IMG/Principal.jpeg' alt='Academia' style='width:1090px;height:552px;'>";
            $img = "<img src='../IMG/Academia-Body.jpg' alt='Academia' style='width:1090px;height:552px;'>";

            parent::adicionaAoCorpo($titulo);
            parent::adicionaAoCorpo($observacao);
            parent::adicionaAoCorpo($img);
        } else {
            // Destrói a sessão por segurança
            session_destroy();
            // Redireciona o visitante de volta pro login
            header('Location: ' . "../Modulos/LoginModulo.php");
            exit;
        }
    }

    public function recebeDados() {
        
    }

    public function recebeDadosDaConsulta() {
        
    }

}
