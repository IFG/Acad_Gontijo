<?php

require_once 'ViewAbstract.php';
require_once '../ADOs/ExercicioAdo.php';
require_once '../ADOs/TipoDeTreinoAdo.php';
require_once '../Models/ExercicioModel.php';
require_once '../Models/TipoDeTreinoModel.php';
require_once '../Classes/MontaHtml.php';

class ExercicioView extends ViewAbstract {

    private function montaOptionsDaConsultaDeExercicios($exerId) {
        $exercicioAdo = new ExercicioAdo();
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();

        $optionsExercicio = null;
        $buscou = $exercicioModel = $exercicioAdo->buscaArrayObjetoComPs(array(), 1, "order by exer_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercício!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Exercício! Contate o analista responsável pelo sistema.");
            }
            $exercicioModel = array();
        }

        foreach ($exercicioModel as $exercicioModel) {
            $selected = null;

            if ($exercicioModel->getExerId() == $exerId) {
                $selected = 1;
            }

            $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaTipoDeTreino($exercicioModel->getTpTrId());

            $text = "Nome: " . $exercicioModel->getExerNome() . " - Tipo de Treino: " . $tipoDeTreinoModel->getTpTrNome();
            $optionsExercicio[] = array("value" => $exercicioModel->getExerId(), "selected" => $selected, "text" => $text);
        }

        return $optionsExercicio;
    }

    private function montaOptionsDeTiposDeTreino($tptrId) {
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();
        $optionsTiposDeTreino = null;
        $buscou = $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tptr_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Tipo de Treino!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Tipo de Treino! Contate o analista responsável pelo sistema.");
            }
            $tipoDeTreinoModel = array();
        }

        foreach ($tipoDeTreinoModel as $tipoDeTreinoModel) {
            $selected = null;

            if ($tipoDeTreinoModel->getTptrId() == $tptrId) {
                $selected = 1;
            }

            $text = "Nome: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTiposDeTreino[] = array("value" => $tipoDeTreinoModel->getTptrId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTiposDeTreino;
    }

    protected function montaHtmlConsulta($exercicioModel) {
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;

        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboExercicios = array("label" => "Exercicios", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeExercicios($exercicioModel->getExerId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboExercicios, $textoPadrao = 'Escolha um Exercicio...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Exercício Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div></form></fieldset>";

        return $htmlConsulta;
    }

    protected function montaCorpo($exercicioModel) {
        $titulo = "<h3>Cadastro de Exercicios</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($exercicioModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($exercicioModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($exercicioModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados do Exercicio</legend>";

        $dadosFieldsetHidden = array("name" => "exerId", "value" => $exercicioModel->getExerId());

        $htmlFieldsetNome = array("label" => "Nome", "type" => "text", "name" => "exerNome", "value" => $exercicioModel->getExerNome(), "class" => "form-control");

        $htmlTextAreaDescricao = array("label" => "Descrição", "type" => "text", "name" => "exerDescricao", "texto" => $exercicioModel->getExerDescricao());

        $htmlComboTiposDeTreino = array("label" => "Tipos de Treino", "name" => "tptrId", "options" => $this->montaOptionsDeTiposDeTreino($exercicioModel->getTptrId()));

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetNome);
        $htmlDados .= "</div><div class='form-group'><div class='col-xs-8'>";
        $htmlDados .= $montaHtml->montaTextArea($htmlTextAreaDescricao, $class = "form-control", $rows = "3");
        $htmlDados .= "</div></div><div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboTiposDeTreino, $textoPadrao = 'Escolha um Tipo de Treino...', null, $class = 'form-control');
        $htmlDados .= "</div></div>";

        $disabled_cad_lim = null;
        if ($exercicioModel->getExerId() != NULL) {
            $disabled_cad_lim = "disabled";
        }

        $disabled_alt_exc = null;
        if ($exercicioModel->getExerId() == NULL) {
            $disabled_alt_exc = "disabled";
        }

        $htmlDados .= "<div class='col-xs-12'></br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' {$disabled_cad_lim} title='Clique para Cadastrar os Dados do Exercício.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            <button name='acao' class='btn btn-warning' type='submit' value='alt' {$disabled_alt_exc} title='Clique para Alterar os Dados do Exercício, Disponível apenas após a Consulta.'><i class='fa fa-pencil-square' aria-hidden='true'></i> Alterar</button>
            <button name='acao' class='btn btn-danger' type='submit' value='exc' {$disabled_alt_exc} title='Clique para Excluir os Dados do Exercício, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button>
            <button name='acao' class='btn btn-primary' type='submit' value='lim' title='Clique para Limpar Todos os Campos.'><i class='fa fa-refresh' aria-hidden='true'></i> Limpar</button>
            </form></fieldset>";

        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {


        $exercicioModel = new ExercicioModel();

        $exercicioModel->setExerId($_POST['idConsulta']);

        return $exercicioModel;
    }

    public function recebeDados() {


        $exercicioModel = new ExercicioModel();

        $exercicioModel->setExerNome($_POST['exerNome']);
        $exercicioModel->setExerDescricao($_POST['exerDescricao']);
        $exercicioModel->setTptrId($_POST['tptrId']);

        return $exercicioModel;
    }

}
