<?php

require_once 'ViewAbstract.php';
require_once '../ADOs/TipoDeTreinoAdo.php';
require_once '../Models/TipoDeTreinoModel.php';
require_once '../Classes/MontaHtml.php';

class TipoDeTreinoView extends ViewAbstract {

    private function montaOptionsDaConsultaDeTiposDeTreino($tptrId) {
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();
        $optionsTiposDeTreino = null;
        $buscou = $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tptr_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercício!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Exercício! Contate o analista responsável pelo sistema.");
            }
            $tipoDeTreinoModel = array();
        }

        foreach ($tipoDeTreinoModel as $tipoDeTreinoModel) {
            $selected = null;

            if ($tipoDeTreinoModel->getTptrId() == $tptrId) {
                $selected = 1;
            }

            $text = "Nome: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTiposDeTreino[] = array("value" => $tipoDeTreinoModel->getTptrId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTiposDeTreino;
    }

    protected function montaHtmlConsulta($tipoDeTreinoModel) {
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;

        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboTiposDeTreino = array("label" => "Tipos de Treino", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeTiposDeTreino($tipoDeTreinoModel->getTptrId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboTiposDeTreino, $textoPadrao = 'Escolha um Tipo de Treino...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Tipo de Treino Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div></form></fieldset>";

        return $htmlConsulta;
    }

    protected function montaCorpo($tipoDeTreinoModel) {
        $titulo = "<h3>Cadastro de Tipos de Treino</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($tipoDeTreinoModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($tipoDeTreinoModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($tipoDeTreinoModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados do Tipo de Treino</legend>";

        $dadosFieldsetHidden = array("name" => "tptrId", "value" => $tipoDeTreinoModel->getTptrId());

        $htmlFieldsetNome = array("label" => "Nome", "type" => "text", "name" => "tptrNome", "value" => $tipoDeTreinoModel->getTptrNome(), "class" => "form-control");

        $htmlTextAreaDescricao = array("label" => "Descrição", "type" => "text", "name" => "tptrDescricao", "texto" => $tipoDeTreinoModel->getTptrDescricao(), "class" => "form-control");

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetNome);
        $htmlDados .= "</div><div class='form-group'><div class='col-xs-8'>";
        $htmlDados .= $montaHtml->montaTextArea($htmlTextAreaDescricao, $class = "form-control", $rows = "3");
        $htmlDados .= "</div></div>";

        $disabled_cad_lim = null;
        if ($tipoDeTreinoModel->getTptrId() != NULL) {
            $disabled_cad_lim = "disabled";
        }

        $disabled_alt_exc = null;
        if ($tipoDeTreinoModel->getTptrId() == NULL) {
            $disabled_alt_exc = "disabled";
        }

        $htmlDados .= "<div class='col-xs-12'></br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' {$disabled_cad_lim} title='Clique para Cadastrar os Dados do Tipo de Treino.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            <button name='acao' class='btn btn-warning' type='submit' value='alt' {$disabled_alt_exc} title='Clique para Alterar os Dados do Tipo de Treino, Disponível apenas após a Consulta.'><i class='fa fa-pencil-square' aria-hidden='true'></i> Alterar</button>
            <button name='acao' class='btn btn-danger' type='submit' value='exc' {$disabled_alt_exc} title='Clique para Excluir os Dados do Tipo de Treino, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button>
            <button name='acao' class='btn btn-primary' type='submit' value='lim' title='Clique para Limpar Todos os Campos.'><i class='fa fa-refresh' aria-hidden='true'></i> Limpar</button>
            </form></fieldset>";

        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {
        $tipoDeTreinoModel = new tipoDeTreinoModel();

        $tipoDeTreinoModel->setTptrId($_POST['idConsulta']);

        return $tipoDeTreinoModel;
    }

    public function recebeDados() {
        $tipoDeTreinoModel = new TipoDeTreinoModel();

        $tipoDeTreinoModel->setTptrId($_POST['tptrId']);
        $tipoDeTreinoModel->setTptrNome($_POST['tptrNome']);
        $tipoDeTreinoModel->setTptrDescricao($_POST['tptrDescricao']);

        return $tipoDeTreinoModel;
    }

}
