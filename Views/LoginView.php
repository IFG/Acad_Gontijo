<?php

require_once 'ViewAbstract.php';
require_once '../Models/UsuarioModel.php';

class LoginView extends ViewAbstract {

    protected function montaCorpo($inicio = null) {
        $titulo = "<center style=color:#fff><h1><b>Bem-Vindo a Tela de Login</b></h1><center>";
        $observacao = "<center style='height:82vh'><h5><b>*Para logar no sistema insira seu login e senha. </b></h5><center>";
        
        $login = "<div class='container'>

        <div class='row' id='pwd-container'>
          <div class='col-md-4'></div>
          
          <div class='col-md-4'>
            <section class='login-form'>
              <form method='post' role='login'>

                <input type='text' name='login' id='login' placeholder='Login' required class='form-control input-lg' value='' />
                <br>
                <input type='password' name='senha' class='form-control input-lg' id='senha' placeholder='Password' required='' /> 
                <br>
                <button type='submit' name='acao' class='btn btn-lg btn-primary btn-block' value='log'>Logar</button>
                
              </form>

              <div class='form-links'>
                www.dcsistemas.com
              </div>
            </section>  
            </div>

            <div class='col-md-4'></div>
            
        </div>";
        
        $limpaMenu = "";
        parent::adicionaAoCorpo($titulo);
        parent::adicionaAoCorpo($observacao);
        parent::adicionaAoCorpo($login);
        parent::adicionaAoMenu($limpaMenu);
    }

    public function recebeDados() {
        $usuarioModel = new UsuarioModel();

        $usuarioModel->setUsuaLogin($_POST['login']);
        $usuarioModel->setUsuaSenha($_POST['senha']);

        return $usuarioModel;
    }

    public function recebeDadosDaConsulta() {
        
    }

}
