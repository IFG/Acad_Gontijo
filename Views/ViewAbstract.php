<?php

abstract class ViewAbstract {

    private $htmlInicio = null;
    private $htmlHead = null;
    private $htmlMenu = null;
    private $htmlCorpo = null;
    private $htmlFim = null;
    private $htmlMensagens = null;
    private $mensagens_sucesso = array();
    private $mensagens_informacao = array();
    private $mensagens_alerta = array();
    private $mensagens_erro = array();

    public function __construct($titulo) {
        $this->montaHtmlInicio($titulo);
        $this->montaHtmlHead();
        $this->montaHtmlMenu();
        $this->montaHtmlFim();
    }

    public function adicionaMensagensDeSucesso($mensagens_sucesso) {
        if (is_array($mensagens_sucesso)) {
            array_merge($this->mensagens_sucesso, $mensagens_sucesso);
        } else {
            $this->mensagens_sucesso[] = $mensagens_sucesso;
        }
    }

    public function adicionaMensagensDeInformacao($mensagens_informacao) {
        if (is_array($mensagens_informacao)) {
            array_merge($this->mensagens_informacao, $mensagens_informacao);
        } else {
            $this->mensagens_informacao[] = $mensagens_informacao;
        }
    }

    public function adicionaMensagensDeAlerta($mensagens_alerta) {
        if (is_array($mensagens_alerta)) {
            array_merge($this->mensagens_alerta, $mensagens_alerta);
        } else {
            $this->mensagens_alerta[] = $mensagens_alerta;
        }
    }

    public function adicionaMensagensDeErro($mensagens_erro) {
        if (is_array($mensagens_erro)) {
            array_merge($this->mensagens_erro, $mensagens_erro);
        } else {
            $this->mensagens_erro[] = $mensagens_erro;
        }
    }

    public function montaHtmlMensagensSucesso() {
        if ($this->mensagens_sucesso) {
            $this->htmlMensagens .= "<div class='modal fade' id='myModal' role='dialog'>
                                    <div class='modal-dialog'>
                                    
                                      <!-- Modal content-->
                                      <div class='modal-content'>
                                        <div class='modal-header alert-success'>
                                          <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                          <h4 class='modal-title'>Cadastrado com Sucesso!</h4>
                                        </div>
                                        <div class='modal-body'>
                                          <p></p>
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                        </div>
                                      </div>

                                    </div>
                                  </div>";
            foreach ($this->mensagens_sucesso as $mensagem) {
                $this->htmlMensagens .= "<script>
                                          var mymodal = $('#myModal');
                                          mymodal.modal();
                                          mymodal.find('.modal-body').html('$mensagem'.bold());
                                          </script>";
            }
        }
    }

    public function montaHtmlMensagensInformacao() {
        if ($this->mensagens_informacao) {
            $this->htmlMensagens .= "<div class='alert alert-info' role='alert'>";
            foreach ($this->mensagens_informacao as $mensagem) {
                $this->htmlMensagens .= "<p><strong>" . $mensagem . "</strong><p>";
            }
            $this->htmlMensagens .= "</div>";
        }
    }

    public function montaHtmlMensagensAlerta() {
        if ($this->mensagens_alerta) {
            $this->htmlMensagens .= "<div class='modal fade' id='myModal' role='dialog'>
                                    <div class='modal-dialog'>
                                    
                                      <!-- Modal content-->
                                      <div class='modal-content'>
                                        <div class='modal-header alert-warning'>
                                          <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                          <h4 class='modal-title'>Alerta!</h4>
                                        </div>
                                        <div class='modal-body'>
                                          <p></p>
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-default' data-dismiss='modal'>Fechar</button>
                                        </div>
                                      </div>

                                    </div>
                                  </div>";
            $texto = null;
            foreach ($this->mensagens_alerta as $mensagem) {
                $texto .= "<br>" . $mensagem . "</br>";
            }
            $this->htmlMensagens .= "<script>
                                        var mymodal = $('#myModal');
                                        mymodal.modal();
                                        mymodal.find('.modal-body').html('$texto'.bold());
                                     </script>";
        }
    }

    public function montaHtmlMensagensErro() {
        if ($this->mensagens_erro) {
            $this->htmlMensagens .= "<div class='modal fade' id='myModal' role='dialog'>
                                    <div class='modal-dialog'>
                                    
                                      <!-- Modal content-->
                                      <div class='modal-content'>
                                        <div class='modal-header alert-danger'>
                                          <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                          <h4 class='modal-title'>Ocorreu um Erro</h4>
                                        </div>
                                        <div class='modal-body'>
                                          <p></p>
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-default' data-dismiss='modal'>Fechar</button>
                                        </div>
                                      </div>

                                    </div>
                                  </div>";
            foreach ($this->mensagens_erro as $mensagem) {
                $this->htmlMensagens .= "<script>
                                          var mymodal = $('#myModal');
                                          mymodal.modal();
                                          mymodal.find('.modal-body').html('$mensagem'.bold());
                                          </script>";
            }
        }
    }

    public function montaHtmlInicio($titulo) {
        session_start();
        
        $this->htmlInicio = "<!DOCTYPE html>"
                . "<html>"
                . "<head>"
                . "<meta charset='utf-8'>"
                . "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"
                . "<title>{$titulo}</title>"
                . "<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>"
                . "<script src='../CSS/plugins/jQuery/jquery-2.2.3.min.js'></script>"
                . "<script src='../CSS/bootstrap/js/bootstrap.js'></script>"
                . "<link rel='stylesheet' href='../CSS/bootstrap/css/bootstrap.css'>"
                . "<link rel='stylesheet' href='../CSS/dist/css/AdminLTE.css'>"
                . "<link rel='stylesheet' href='../CSS/dist/css/skins/_all-skins.css'>"
                . "<link rel='stylesheet' href='../CSS/plugins/datepicker/datepicker3.css'>"
                . "<link rel='stylesheet' href='../CSS/font-icone/css/font-awesome.css'>"
                . "<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.2.0/css/all.css' integrity='sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ' crossorigin='anonymous'>"
                . "</head>";
    }

    public function montaHtmlHead() {
        
        if (empty($_SESSION['nome'])) {
            $this->htmlHead = "<body class='hold-transition skin-purple sidebar-mini'>"
                    . "<div class='wrapper'>"
                    . "<header class='main-header'>"
                    . "<a href='InicioModulo.php' class='logo'>"
                    . "<span class='logo-mini'><b>A</b>GT</span>"
                    . "<span class='logo-lg'><b>Academia </b>Gontijo</span>"
                    . "</a>"
                    . "<nav class='navbar navbar-static-top'>"
                    . "<a href='#' class='sidebar-toggle' data-toggle='offcanvas' role='button'>"
                    . "<span class='sr-only'>Toggle navigation</span>"
                    . "</a>"
                    . "<div class='navbar-custom-menu'>"
                    . "<ul class='nav navbar-nav'>"
                    . "<li>"
                    . "<a href='LoginModulo.php'>"
                    . "<i class='fas fa-user'></i></a>"
                    . "</li>"
                    . "</ul>"
                    . "</div>"
                    . "</nav>"
                    . "</header>";
        } else {
            $this->htmlHead = "<body class='hold-transition skin-purple sidebar-mini'>"
                    . "<div class='wrapper'>"
                    . "<header class='main-header'>"
                    . "<a href='InicioModulo.php' class='logo'>"
                    . "<span class='logo-mini'><b>A</b>GT</span>"
                    . "<span class='logo-lg'><b>Academia </b>Gontijo</span>"
                    . "</a>"
                    . "<nav class='navbar navbar-static-top'>"
                    . "<a href='#' class='sidebar-toggle' data-toggle='offcanvas' role='button'>"
                    . "<span class='sr-only'>Toggle navigation</span>"
                    . "</a>"
                    . "<div class='navbar-custom-menu'>"
                    . "<ul class='nav navbar-nav'>"
                    . "<li>"
                    . "<center style='color: #fff'><b>{$_SESSION['nome']}</b>"
                    . "<br>"
                    . "<a href='../Classes/logout.php'>Sair</a>"
                    . "</li>"
                    . "</ul>"
                    . "</div>"
                    . "</nav>"
                    . "</header>";
        }
    }

    public function montaHtmlMenu() {
        
        $this->htmlMenu = "<aside class='main-sidebar'>"
                . "<section class='sidebar'>"
                . "<ul class='sidebar-menu'>"
                . "<li class='header'><center style='font-size:18px'>MENU</center></li>"
                . "<li class='treeview'>"
                . "<a href='InicioModulo.php'>"
                . "<i class='fa fa-home'></i> "
                . "<span title='Página Inicial do Sistema'>Início</span>"
                . "<span class='pull-right-container'>"
                . "</span>"
                . "</a>"
                . "</li>"
                . "<li class='treeview'>"
                . "<a href='#'>"
                . "<i class='fa fa-address-card'></i> "
                . "<span>Cadastros</span>"
                . "<span class='pull-right-container'>"
                . "<i class='fa fa-angle-left pull-right'></i>"
                . "</span>"
                . "</a>"
                . "<ul class='treeview-menu'>";
        
        if(empty($_SESSION['nivel'])){
            //não faz nada
        }else{
            if($_SESSION['nivel'] == 'S'){
                $this->htmlMenu .= "<li><a href='AtletaModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo atleta no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Atletas</a></li>"
                . "<li><a href='TreinadorModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo treinador no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Treinadores</a></li>"
                . "</ul>"
                . "</li>"
                . "<span class='pull-right-container'>"
                . "<i class='fa fa-angle-left pull-right'></i>"
                . "</span>"
                . "</a>"
                . "</section>"
                . "</aside>"
                . "<div class='content-wrapper'>"
                . "<section class='content'>";
            }if($_SESSION['nivel'] == 'P'){
                $this->htmlMenu .= "<li><a href='ExercicioModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo exercícios no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Exercícios</a></li>"
                . "<li><a href='TipoDeTreinoModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo atleta no sistema.'><i class='fa fa-dot-circle-o'></i>Cad. de Tipos de Treinos</a></li>"
                . "</ul>"
                . "</li>"
                . "<li class='treeview'>"
                . "<a href='#'>"
                . "<i class='fa  fa-bicycle'></i>"
                . "<span>Treino</span>"
                . "<span class='pull-right-container'>"
                . "<i class='fa fa-angle-left pull-right'></i>"
                . "</span>"
                . "</a>"
                . "<ul class='treeview-menu'>"
                . "<li><a href='TreinoModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo treino.'><i class='fa fa-dot-circle-o'></i>Cadastro de Treino</a></li>"
                . "<li><a href='TreinoExercicioModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo exercicio de um treino.'><i class='fa fa-dot-circle-o'></i>Cad. de Exercícios do Treino</a></li>"
                . "</ul>"
                . "</li>"
                . "</ul>"
                . "</section>"
                . "</aside>"
                . "<div class='content-wrapper'>"
                . "<section class='content'>";
            }if($_SESSION['nivel'] == 'A'){
                $this->htmlMenu .= "<li><a href='AtletaModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo atleta no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Atletas</a></li>"
                . "<li><a href='TreinadorModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo treinador no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Treinadores</a></li>"
                . "<li><a href='ExercicioModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo exercícios no sistema.'><i class='fa fa-dot-circle-o'></i>Cadastro de Exercícios</a></li>"
                . "<li><a href='TipoDeTreinoModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo atleta no sistema.'><i class='fa fa-dot-circle-o'></i>Cad. de Tipos de Treinos</a></li>"
                . "</ul>"
                . "</li>"
                . "<li class='treeview'>"
                . "<a href='#'>"
                . "<i class='fa  fa-bicycle'></i>"
                . "<span>Treino</span>"
                . "<span class='pull-right-container'>"
                . "<i class='fa fa-angle-left pull-right'></i>"
                . "</span>"
                . "</a>"
                . "<ul class='treeview-menu'>"
                . "<li><a href='TreinoModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo treino.'><i class='fa fa-dot-circle-o'></i>Cadastro de Treino</a></li>"
                . "<li><a href='TreinoExercicioModulo.php' title='Permite consultar, cadastrar, alterar e excluir um novo exercicio de um treino.'><i class='fa fa-dot-circle-o'></i>Cad. de Exercícios do Treino</a></li>"
                . "</ul>"
                . "</li>"
                . "</ul>"
                . "</section>"
                . "</aside>"
                . "<div class='content-wrapper'>"
                . "<section class='content'>";
            }
        }
    }

    abstract protected function montaCorpo($objeto);

    public function adicionaAoCorpo($codigo) {
        $this->htmlCorpo .= $codigo;
    }

    public function adicionaAoMenu($codigo) {
        $this->htmlMenu = $codigo;
    }

    public function displayInterface($objeto) {
        $this->montaCorpo($objeto);
        $this->montaHtmlMensagensSucesso();
        $this->montaHtmlMensagensInformacao();
        $this->montaHtmlMensagensAlerta();
        $this->montaHtmlMensagensErro();

        echo $this->htmlInicio . $this->htmlHead . $this->htmlMenu . $this->htmlMensagens . $this->htmlCorpo . $this->htmlFim;
    }

    public function montaHtmlFim() {

        $this->htmlFim = "</section>"
                . "</div>"
                . "<script src='../JS/jquery.maskedinput.js'></script>"
                . "<script src='../CSS/plugins/input-mask/jquery.inputmask.js'></script>"
                . "<script src='../CSS/plugins/input-mask/jquery.maskMoney.js'></script>"
                . "<script src='../CSS/plugins/iCheck/icheck.js'></script>"
                . "<script src='../CSS/plugins/datepicker/bootstrap-datepicker.js'></script>"
                . "<script src='../JS/Locales/bootstrap-datepicker.pt-BR.js'></script>"
                . "<script src='../CSS/plugins/select2/select2.js'></script>"
                . "<script src='../CSS/dist/js/app.js'></script>"
                . "<script src='../JS/mascaraJQuery.js'></script>"
                . "<script src='../JS/AJAX/ExercicioAjax.js'></script>"
                . "</body>"
                . "</html>";
    }

    public function getAcao() {
        if (isset($_POST['acao'])) {
            return $_POST['acao'];
        } else {
            return 'nov';
        }
    }

    abstract public function recebeDadosDaConsulta();

    abstract public function recebeDados();
}
