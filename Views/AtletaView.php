<?php

require_once 'ViewAbstract.php';
require_once '../ADOs/AtletaAdo.php';
require_once '../ADOs/TreinadorAdo.php';
require_once '../Models/AtletaModel.php';
require_once '../Models/TreinadorModel.php';
require_once '../Classes/MontaHtml.php';

class AtletaView extends ViewAbstract {

    private function montaOptionsDaConsultaDeAtletas($atleId) {
        $atletaAdo = new AtletaAdo();
        $optionsAtletas = null;
        $buscou = $atletaModel = $atletaAdo->buscaArrayObjetoComPs(array(), 1, "order by atle_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Atleta!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Atleta! Contate o analista responsável pelo sistema.");
            }
            $atletaModel = array();
        }

        foreach ($atletaModel as $atletaModel) {
            $selected = null;

            if ($atletaModel->getAtleId() == $atleId) {
                $selected = 1;
            }

            $text = "Nome: " . $atletaModel->getAtleNome() . " - CPF: " . $atletaModel->getAtleCPF();
            $optionsAtletas[] = array("value" => $atletaModel->getAtleId(), "selected" => $selected, "text" => $text);
        }

        return $optionsAtletas;
    }

    private function montaOptionsDeTreinadores($treiId) {
        $treinadorAdo = new TreinadorAdo();
        $optionsTreinadores = null;
        $buscou = $treinadorModel = $treinadorAdo->buscaArrayObjetoComPs(array(), 1, "order by trei_nome");

        if (!$buscou) {
            if ($buscou === 0) {

                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Treinador!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Treinador! Contate o analista responsável pelo sistema.");
            }
            $treinadorModel = array();
        }

        foreach ($treinadorModel as $treinadorModel) {
            $selected = null;

            if ($treinadorModel->getTreiId() == $treiId) {
                $selected = 1;
            }

            $text = "Nome: " . $treinadorModel->getTreiNome();
            $optionsTreinadores[] = array("value" => $treinadorModel->getTreiId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTreinadores;
    }

    protected function montaHtmlConsulta($atletaModel) {
        
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;

        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboAtletas = array("label" => "Atletas", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeAtletas($atletaModel->getAtleId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboAtletas, $textoPadrao = 'Escolha um Atleta...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Atleta Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div></form></fieldset>";

        return $htmlConsulta;
    }

    protected function montaCorpo($atletaModel) {
        $titulo = "<h3>Cadastro de Atletas</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($atletaModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($atletaModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($atletaModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados do Atleta</legend>";

        $dadosFieldsetHidden = array("name" => "atleId", "value" => $atletaModel->getAtleId());

        $htmlComboTreinadores = array("label" => "Treinador", "name" => "treiId", "options" => $this->montaOptionsDeTreinadores($atletaModel->getTreiId()));

        $htmlFieldsetNome = array("label" => "Nome", "type" => "text", "name" => "atleNome", "value" => $atletaModel->getAtleNome(), "class" => "form-control");

        $htmlFieldsetCpf = array("label" => "CPF", "type" => "text", "name" => "atleCpf", "value" => $atletaModel->getAtleCpf(), "class" => "mascaraCPF form-control");

        $htmlFieldsetDataNascimento = array("label" => "Data de Nascimento", "type" => "text", "name" => "atleDataNascimento", "value" => DatasEHoras::getDataDesinvertidaComBarras($atletaModel->getAtleDataNascimento()), "class" => "mascaraData datepicker form-control");

        $htmlFieldsetAltura = array("label" => "Altura", "type" => "text", "name" => "atleAltura", "value" => $atletaModel->getAtleAltura(), "class" => "peso-altura form-control");

        $htmlFieldsetPeso = array("label" => "Peso", "type" => "text", "name" => "atlePeso", "value" => $atletaModel->getAtlePeso(), "class" => "peso-altura form-control");

        $m = $f = 0;
        if ($atletaModel->getAtleSexo() == "F") {
            $f = 1;
        } else {
            $m = 1;
        }

        $htmRadioSexo = array("label" => "Sexo", "buttons" => array
                (array("name" => "atleSexo", "value" => "M", "checked" => $m, "text" => "Masculino"),
                array("name" => "atleSexo", "value" => "F", "checked" => $f, "text" => "Feminino")));

        $htmRadioPretencao = array("label" => "Pretensão", "buttons" => array
                (array("name" => "atlePretensao", "value" => "1", "checked" => 1, "text" => "Musculação"),
                array("name" => "atlePretensao", "value" => "2", "checked" => 0, "text" => "Emagracer"),
                array("name" => "atlePretensao", "value" => "3", "checked" => 0, "text" => "Engordar"),
                array("name" => "atlePretensao", "value" => "4", "checked" => 0, "text" => "Manter a Forma")));

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboTreinadores, $textoPadrao = 'Escolha um Treinador...', null, $class = 'form-control');
        $htmlDados .= "</div></div><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetNome);
        $htmlDados .= "</div><div class='col-xs-3'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetCpf);
        $htmlDados .= "</div><div class='col-xs-3'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetDataNascimento);
        $htmlDados .= "</div><div class='col-xs-2'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetAltura);
        $htmlDados .= "</div><div class='col-xs-2'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetPeso);
        $htmlDados .= "</div><div class='col-xs-3'>";
        $htmlDados .= $montaHtml->montaRadioEmLinha($htmRadioSexo);
        $htmlDados .= "</div><div class='col-xs-5'>";
        $htmlDados .= $montaHtml->montaRadioEmLinha($htmRadioPretencao);
        $htmlDados .= "</div>";

        $disabled_cad_lim = null;
        if ($atletaModel->getAtleId() != NULL) {
            $disabled_cad_lim = "disabled";
        }

        $disabled_alt_exc = null;
        if ($atletaModel->getAtleId() == NULL) {
            $disabled_alt_exc = "disabled";
        }

        $htmlDados .= "<div class='col-xs-12'></br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' {$disabled_cad_lim} title='Clique para Cadastrar os Dados do Atleta.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            <button name='acao' class='btn btn-warning' type='submit' value='alt' {$disabled_alt_exc} title='Clique para Alterar os Dados do Atleta, Disponível apenas após a Consulta.'><i class='fa fa-pencil-square' aria-hidden='true'></i> Alterar</button>
            <button name='acao' class='btn btn-danger' type='submit' value='exc' {$disabled_alt_exc} title='Clique para Excluir os Dados do Atleta, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button>
            <button name='acao' class='btn btn-primary' type='submit' value='lim' title='Clique para Limpar Todos os Campos.'><i class='fas fa-broom' aria-hidden='true'></i> Limpar</button>
            </form></fieldset>";

        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {
        $atletaModel = new AtletaModel();

        $atletaModel->setAtleId($_POST['idConsulta']);

        return $atletaModel;
    }

    public function recebeDados() {
        $atletaModel = new AtletaModel();

        $atletaModel->setAtleId($_POST['atleId']);
        $atletaModel->setAtleNome($_POST['atleNome']);
        $atletaModel->setAtleCpf($_POST['atleCpf']);
        $atletaModel->setAtleDataNascimento(DatasEHoras::getDataInvertidaComTracos($_POST['atleDataNascimento']));
        $atletaModel->setAtleAltura($_POST['atleAltura']);
        $atletaModel->setAtlePeso($_POST['atlePeso']);
        $atletaModel->setAtleSexo($_POST['atleSexo']);
        $atletaModel->setAtlePretensao($_POST['atlePretensao']);
        $atletaModel->setTreiId($_POST['treiId']);

        return $atletaModel;
    }

}
