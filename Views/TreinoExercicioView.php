<?php

require_once 'ViewAbstract.php';
require_once '../Models/TreinoExercicioModel.php';
require_once '../Models/TreinoModel.php';
require_once '../Models/ExercicioModel.php';
require_once '../Models/AtletaModel.php';
require_once '../Models/TipoDeTreinoModel.php';
require_once '../ADOs/TreinoExercicioAdo.php';
require_once '../ADOs/TreinoAdo.php';
require_once '../ADOs/ExercicioAdo.php';
require_once '../ADOs/AtletaAdo.php';
require_once '../ADOs/TipoDeTreinoAdo.php';
require_once '../Classes/MontaHtml.php';

class TreinoExercicioView extends ViewAbstract {

    private function montaOptionsDaConsultaDeTreinoExercicio($trenId) {
        $treinoAdo = new TreinoAdo();
        $atletaAdo = new AtletaAdo();
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();

        $optionsTreinos = null;
        $buscou = $treinoModel = $treinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tren_seq");


        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercício do Treino!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Treino! Contate o analista responsável pelo sistema.");
            }
            $treinoModel = array();
        }

        foreach ($treinoModel as $treinoModel) {
            $selected = null;
            if ($treinoModel->getTrenId() == $trenId) {
                $selected = 1;
            }

            $atletaModel = $atletaAdo->buscaAtleta($treinoModel->getAtleId());
            $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaTipoDeTreino($treinoModel->getTptrId());

            $text = "Sequência: " . $treinoModel->getTrenSeq() . " - Atleta: " . $atletaModel->getAtleNome() . " - CPF: " . $atletaModel->getAtleCPF() . " - Tipo de Treino: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTreinos[] = array("value" => $treinoModel->getTrenId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTreinos;
    }

    private function montaOptionsDeTreinos($trenId) {
        $treinoAdo = new TreinoAdo();
        $atletaAdo = new AtletaAdo();
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();

        $optionsTreinos = null;
        $buscou = $treinoModel = $treinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tren_seq");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Treino!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Treino! Contate o analista responsável pelo sistema.");
            }
            $treinoModel = array();
        }

        foreach ($treinoModel as $treinoModel) {
            $selected = null;
            if ($treinoModel->getTrenId() == $trenId) {
                $selected = 1;
            }

            $atletaModel = $atletaAdo->buscaAtleta($treinoModel->getAtleId());
            $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaTipoDeTreino($treinoModel->getTptrId());

            $text = "Sequência: " . $treinoModel->getTrenSeq() . " - Atleta: " . $atletaModel->getAtleNome() . " - CPF: " . $atletaModel->getAtleCPF() . " - Tipo de Treino: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTreinos[] = array("value" => $treinoModel->getTrenId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTreinos;
    }

    private function montaOptionsDeExercicios($exerId) {
        $exercicioAdo = new ExercicioAdo();
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();

        $optionsExercicio = null;
        $buscou = $exercicioModel = $exercicioAdo->buscaArrayObjetoComPs(array(), 1, "order by exer_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercício!");
            } else {
                //parent::adicionaMensagensDeErro("Erro ao Buscar Exercício! Contate o analista responsável pelo sistema.");
            }
            $exercicioModel = array();
        }

        foreach ($exercicioModel as $exercicioModel) {
            $selected = null;

            if ($exercicioModel->getExerId() == $exerId) {
                $selected = 1;
            }

            $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaTipoDeTreino($exercicioModel->getTpTrId());

            $text = "Nome: " . $exercicioModel->getExerNome() . " - Tipo de Treino: " . $tipoDeTreinoModel->getTpTrNome();
            $optionsExercicio[] = array("value" => $exercicioModel->getExerId(), "selected" => $selected, "text" => $text);
        }

        return $optionsExercicio;
    }

    private function montaTabelaDeExercicios($trenId) {
        if ($trenId == NULL) {
            return NULL;
        }

        $htmlTabela = null;
        $montaHtml = new MontaHTML();
        $exercicioAdo = new ExercicioAdo();
        $treinoExercicioAdo = new TreinoExercicioAdo();
        $buscou = $treinoExercicioModel = $treinoExercicioAdo->buscaArrayObjetoComPs(array($trenId), "tren_id = ?", "order by trex_id");

        if (!$buscou) {
            if ($buscou === 0) {
//                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercício no Treino!");
            } else {
//                parent::adicionaMensagensDeErro("Erro ao Buscar Exercício! Contate o analista responsável pelo sistema.");
            }

            $treinoExercicioModel = array();
        }

        $htmlTabela .= "<div class='row'>"
                . "<div class='col-xs-12'>"
                . "<br><div class='box'>"
                . "<div class='box-header'>"
                . "<h3 class='box-title'>Treinamento</h3>"
                . "</div>"
                . "<div class='box-body table-responsive no-padding'>";

        $htmlTabela .= "<table class='table table-hover'>";
        $htmlTabela .= "<tr>";
        $htmlTabela .= "<th>Exercício</th>";
        $htmlTabela .= "<th>Tempo</th>";
        $htmlTabela .= "<th>Série</th>";
        $htmlTabela .= "<th>Repetição</th>";
        $htmlTabela .= "<th>Excluir</th>";
        $htmlTabela .= "</tr>";

        foreach ($treinoExercicioModel as $treinoExercicioModel) {
            $exercicioModel = $exercicioAdo->buscaExercicio($treinoExercicioModel->getExerId());

            $htmlTabela .= "<tr>";
            $htmlTabela .= "<th>" . $exercicioModel->getExerNome() . "</th>";
            $htmlTabela .= "<th>" . $treinoExercicioModel->getTrexTempo() . "</th>";
            $htmlTabela .= "<th>" . $treinoExercicioModel->getTrexSerie() . "</th>";
            $htmlTabela .= "<th>" . $treinoExercicioModel->getTrexRepeticao() . "</th>";
            $dadosFieldsetHidden = array("name" => "trexTbId", "value" => $treinoExercicioModel->getTrexId());
            $htmlTabela .= "<th><form id='form' action='' method='POST'>" . $montaHtml->montaInputHidden($dadosFieldsetHidden) . "<button name='acao' class='btn btn-danger' type='submit' value='exc' title='Clique para Alterar os Dados do Exercícios de Treino, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button></form></th>";
            $htmlTabela .= "</tr>";
        }

        $htmlTabela .= "</table>"
                . "</div>"
                . "</div>"
                . "</div>"
                . "</div>";

        return $htmlTabela;
    }

    protected function montaHtmlConsulta($treinoExercicioModel) {
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;
        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboTreinoExercicio = array("label" => "Treino", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeTreinoExercicio($treinoExercicioModel->getTrenId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboTreinoExercicio, $textoPadrao = 'Escolha um Treino Exercicio...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Exercícios do Treino Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div></form></fieldset>";

        return $htmlConsulta;
    }

    protected function montaCorpo($treinoExercicioModel) {

        $titulo = "<h3>Cadastro de Exercícios do Treino</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($treinoExercicioModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($treinoExercicioModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($treinoExercicioModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados dos Exercícios do Treino</legend>";

        $dadosFieldsetHidden = array("name" => "trexId", "value" => null);

        $htmlFieldsetTempo = array("label" => "Tempo", "type" => "text", "name" => "trexTempo", "value" => null, "class" => "num-inteiro form-control");

        $htmlFieldsetSerie = array("label" => "Serie", "type" => "text", "name" => "trexSerie", "value" => null, "class" => "num-inteiro form-control");

        $htmlFieldsetRepeticao = array("label" => "Repeticao", "type" => "text", "name" => "trexRepeticao", "value" => null, "class" => "num-inteiro form-control");

        $htmlComboTreino = array("label" => "Treino", "name" => "trenId", "options" => $this->montaOptionsDeTreinos(null));

        $htmlComboExercicio = array("label" => "Exercício", "name" => "exerId", "options" => $this->montaOptionsDeExercicios(null));

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboTreino, $textoPadrao = 'Escolha um Treino...', $onChange = "onchange=\"ExercicioAjax()\"", $class = 'form-control');
        $htmlDados .= "</div></div><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboExercicio, $textoPadrao = 'Escolha um Exercicio...', null, $class = 'form-control');
        $htmlDados .= "</div><div class='col-xs-2'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetTempo);
        $htmlDados .= "</div><div class='col-xs-2'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetSerie);
        $htmlDados .= "</div><div class='col-xs-2'>";
        $htmlDados .= $montaHtml->montaInput($htmlFieldsetRepeticao);
        $htmlDados .= "</div>";

        $htmlDados .= "<div class='col-xs-12'><br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' title='Clique para Cadastrar os Dados do Exercícios do Treino.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            </div>";

        $htmlDados .= $this->montaTabelaDeExercicios($treinoExercicioModel->getTrenId());

        $htmlDados .= "</form></fieldset>";

        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {
        $treinoExercicioModel = new TreinoExercicioModel();

        $treinoExercicioModel->setTrenId($_POST['idConsulta']);

        return $treinoExercicioModel;
    }

    public function recebeDados() {
        $treinoExercicioModel = new TreinoExercicioModel();

        $treinoExercicioModel->setTrexId($_POST['trexId']);
        $treinoExercicioModel->setTrexTempo($_POST['trexTempo']);
        $treinoExercicioModel->setTrexSerie($_POST['trexSerie']);
        $treinoExercicioModel->setTrexRepeticao($_POST['trexRepeticao']);
        $treinoExercicioModel->setTrenId($_POST['trenId']);
        $treinoExercicioModel->setExerId($_POST['exerId']);

        return $treinoExercicioModel;
    }

}
