<?php

require_once 'ViewAbstract.php';
require_once '../ADOs/TipoDeTreinoAdo.php';
require_once '../ADOs/TreinadorAdo.php';
require_once '../ADOs/AtletaAdo.php';
require_once '../ADOs/ExercicioAdo.php';
require_once '../Models/TreinadorModel.php';
require_once '../Models/AtletaModel.php';
require_once '../Models/ExercicioModel.php';
require_once '../Models/TipoDeTreinoModel.php';
require_once '../Classes/MontaHtml.php';

class TreinoView extends ViewAbstract {

    private function montaOptionsDaConsultaDeTreinos($trenId) {
        $treinoAdo = new TreinoAdo();
        $atletaAdo = new AtletaAdo();
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();

        $optionsTreinos = null;
        $buscou = $treinoModel = $treinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tren_seq");


        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Treino!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Treino! Contate o analista responsável pelo sistema.");
            }
            $treinoModel = array();
        }

        foreach ($treinoModel as $treinoModel) {
            $selected = null;
            if ($treinoModel->getTrenId() == $trenId) {
                $selected = 1;
            }

            $atletaModel = $atletaAdo->buscaAtleta($treinoModel->getAtleId());
            $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaTipoDeTreino($treinoModel->getTptrId());

            $text = "Sequência: " . $treinoModel->getTrenSeq() . " - Atleta: " . $atletaModel->getAtleNome() . " - CPF: " . $atletaModel->getAtleCPF() . " - Tipo de Treino: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTreinos[] = array("value" => $treinoModel->getTrenId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTreinos;
    }

    private function montaOptionsDeSequencia($treiSeq) {

        $arraySequencia = array();
        $arraySequencia[] = array("value" => 1, "text" => "Sequencia 1");
        $arraySequencia[] = array("value" => 2, "text" => "Sequencia 2");
        $arraySequencia[] = array("value" => 3, "text" => "Sequencia 3");

        foreach ($arraySequencia as $arraySequencia) {
            $selected = null;

            if ($arraySequencia["value"] == $treiSeq) {
                $selected = 1;
            }

            $text = "Sequência: " . $arraySequencia["text"];
            $optionsSequencia[] = array("value" => $arraySequencia["value"], "selected" => $selected, "text" => $text);
        }

        return $optionsSequencia;
    }

    private function montaOptionsDeAtletas($atleId) {
        $atletaAdo = new AtletaAdo();
        $optionsAtletas = null;
        $buscou = $atletaModel = $atletaAdo->buscaArrayObjetoComPs(array(), 1, "order by atle_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Atleta!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Atleta! Contate o analista responsável pelo sistema.");
            }
            $atletaModel = array();
        }

        foreach ($atletaModel as $atletaModel) {
            $selected = null;

            if ($atletaModel->getAtleId() == $atleId) {
                $selected = 1;
            }

            $text = "Nome: " . $atletaModel->getAtleNome() . " - CPF: " . $atletaModel->getAtleCPF();
            $optionsAtletas[] = array("value" => $atletaModel->getAtleId(), "selected" => $selected, "text" => $text);
        }

        return $optionsAtletas;
    }

    private function montaOptionsDeTiposDeTreino($tptrId) {
        $tipoDeTreinoAdo = new TipoDeTreinoAdo();
        $optionsTiposDeTreino = null;
        $buscou = $tipoDeTreinoModel = $tipoDeTreinoAdo->buscaArrayObjetoComPs(array(), 1, "order by tptr_nome");

        if (!$buscou) {
            if ($buscou === 0) {
                parent::adicionaMensagensDeInformacao("Não foi possível encontrar nenhum Exercicio!");
            } else {
                parent::adicionaMensagensDeErro("Erro ao Buscar Exercicio! Contate o analista responsável pelo sistema.");
            }
            $tipoDeTreinoModel = array();
        }

        foreach ($tipoDeTreinoModel as $tipoDeTreinoModel) {
            $selected = null;

            if ($tipoDeTreinoModel->getTptrId() == $tptrId) {
                $selected = 1;
            }

            $text = "Nome: " . $tipoDeTreinoModel->getTptrNome();
            $optionsTiposDeTreino[] = array("value" => $tipoDeTreinoModel->getTptrId(), "selected" => $selected, "text" => $text);
        }

        return $optionsTiposDeTreino;
    }

    protected function montaHtmlConsulta($treinoModel) {
        $montaHtml = new MontaHTML();
        $htmlConsulta = null;

        $htmlConsulta .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Consulta</legend>";

        $htmlComboTreinos = array("label" => "Treino", "name" => "idConsulta", "options" => $this->montaOptionsDaConsultaDeTreinos($treinoModel->getTrenId()));
        $htmlConsulta .= "<div class='row'>";
        $htmlConsulta .= "<div class='col-xs-10'>";
        $htmlConsulta .= $montaHtml->montaCombobox($htmlComboTreinos, $textoPadrao = 'Escolha um Treino...', null, $class = 'form-control');
        $htmlConsulta .= "</div></div><p><div class='col-xs-6'>";
        $htmlConsulta .= "<button class='btn btn-info' name='acao' type='submit' value='con' title='Clique para Consultar os Dados do Treino Selecionado.'><i class='fa fa-search' aria-hidden='true'></i>  Consultar</button>";
        $htmlConsulta .= "</div></form></fieldset>";


        return $htmlConsulta;
    }

    protected function montaCorpo($treinoModel) {
        $titulo = "<h3>Cadastro de Treinos</h3>";

        parent::adicionaAoCorpo($titulo);

        $htmlConsulta = $this->montaHtmlConsulta($treinoModel);
        parent::adicionaAoCorpo($htmlConsulta);

        $htmlDados = $this->montaHtmlDados($treinoModel);
        parent::adicionaAoCorpo($htmlDados);
    }

    protected function montaHtmlDados($treinoModel) {
        $montaHtml = new MontaHTML();
        $htmlDados = null;

        $htmlDados .= "<form id='form' action='' method='POST'>"
                . "<fieldset>"
                . "<legend>Dados do Treino</legend>";

        $dadosFieldsetHidden = array("name" => "trenId", "value" => $treinoModel->getTrenId());

        $htmlComboSequencia = array("label" => "Sequencia", "name" => "trenSequencia", "options" => $this->montaOptionsDeSequencia($treinoModel->getTrenSeq()));

        $htmlComboAtletas = array("label" => "Atleta", "name" => "atleId", "options" => $this->montaOptionsDeAtletas($treinoModel->getAtleId()));

        $htmlComboTipoDeTreinos = array("label" => "Tipo de Treino", "name" => "tptrId", "options" => $this->montaOptionsDeTiposDeTreino($treinoModel->getTptrId()));

        $htmlDados .= $montaHtml->montaInputHidden($dadosFieldsetHidden);
        $htmlDados .= "<div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboSequencia, $textoPadrao = 'Escolha uma Sequencia...', null, $class = 'form-control');
        $htmlDados .= "</div></div><div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboAtletas, $textoPadrao = 'Escolha um Atleta...', null, $class = 'form-control');
        $htmlDados .= "</div></div><div class='row'><div class='col-xs-6'>";
        $htmlDados .= $montaHtml->montaCombobox($htmlComboTipoDeTreinos, $textoPadrao = 'Escolha um Tipo de Treino...', null, $class = 'form-control');
        $htmlDados .= "</div></div>";

        $disabled_cad_lim = null;
        if ($treinoModel->getTrenId() != NULL) {
            $disabled_cad_lim = "disabled";
        }

        $disabled_alt_exc = null;
        if ($treinoModel->getTrenId() == NULL) {
            $disabled_alt_exc = "disabled";
        }

        $htmlDados .= "<div class='col-xs-12'></br>
            <button name='acao' class='btn btn-success' type='submit' value='cad' {$disabled_cad_lim} title='Clique para Cadastrar os Dados do Treino.'><i class='fa fa-check-square' aria-hidden='true'></i> Cadastrar</button>
            <button name='acao' class='btn btn-warning' type='submit' value='alt' {$disabled_alt_exc} title='Clique para Alterar os Dados do Treino, Disponível apenas após a Consulta.'><i class='fa fa-pencil-square' aria-hidden='true'></i> Alterar</button>
            <button name='acao' class='btn btn-danger' type='submit' value='exc' {$disabled_alt_exc} title='Clique para Excluir os Dados do Treino, Disponível apenas após a Consulta.'><i class='fa fa-trash' aria-hidden='true'></i> Excluir</button>
            <button name='acao' class='btn btn-primary' type='submit' value='lim' title='Clique para Limpar Todos os Campos.'><i class='fa fa-refresh' aria-hidden='true'></i> Limpar</button>
            </form></fieldset>";


        return $htmlDados;
    }

    public function recebeDadosDaConsulta() {
        $treinoModel = new TreinoModel();

        $treinoModel->setTrenId($_POST['idConsulta']);

        return $treinoModel;
    }

    public function recebeDados() {
        $treinoModel = new TreinoModel();

        $treinoModel->setTrenId($_POST['trenId']);
        $treinoModel->setTrenSeq($_POST['trenSequencia']);
        $treinoModel->setAtleId($_POST['atleId']);
        $treinoModel->setTptrId($_POST['tptrId']);
        
        return $treinoModel;
    }

}
