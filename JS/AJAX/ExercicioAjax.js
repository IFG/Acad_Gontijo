
function ExercicioAjax() {

    var ajax = $.ajax({
        url: "../AJAX/ExercicioAjax.php",
        type: "POST",
        data: {
            trenId: $("#trenId").val()
        }
    });

    ajax.done(function (resultado) {
        var valorRetornado = resultado.split("|");
        if (valorRetornado[0] == 'add') {
            var select = document.getElementById("exerId");
            select.options.length = 0;
            for (i = 1; i < valorRetornado.length; i = i + 2) {
                var novaOpcao = new Option(valorRetornado[i + 1] /*Texto*/, valorRetornado[i] /*Value*/);
                select.options.add(novaOpcao, undefined);
            }
        }
        if (valorRetornado[0] == 'erro') {
            alert(valorRetornado[1]);
        }
    });

    ajax.fail(function (erro) {
        alert("Erro no AJAX. Contate o analista! ");
    });

}