
$(document).ready(function () {
    $('.mascaraDateTime').inputmask('99/99/9999 99:99'); //data dateTimePicker
    $('.mascaraData').inputmask('99/99/9999'); //data dateTimePicker
    $('.mascaraHora').inputmask('99:99:99'); //horas
    // CSB - 02/05/2016 - Comentado para ser reescrito nas linhas abaixo
    $('.mascaraTelefone').inputmask('(99) 9999-9999'); //telefone
    $('.mascaraRG').inputmask('99.999.999-9'); //RG
    $('.mascaraCPF').inputmask('999.999.999-99'); //CPF
    $('.mascaraCodigoArea').inputmask('9.99.99.99-9'); //Código de Grandes Áreas/Áreas/Sub-Áreas
    $('.mascaraAno').inputmask('9999'); //máscara para ano. ex: 2013
    $('.mascaraCEP').inputmask('99999-999');
    $('.mascaraMatricula').inputmask('99999999999999');
    $('.mascaraPeriodo').inputmask('99');//máscara para períodos de discentes. Apenas números.
    $('.mascaraNotaAvaliacao').inputmask('99,99');//máscara para a nota de avaliação de projetos. No programa aprovareprovaprojetos.php
    $('.mascaraIsbn').inputmask('999-99-9999-999-9');//máscara para a nota de avaliação de projetos. No programa aprovareprovaprojetos.php

//    $.datepicker.regional['pt-BR'] = {
//        closeText: 'Fechar',
//        prevText: '&#x3c;Anterior',
//        nextText: 'Pr&oacute;ximo&#x3e;',
//        currentText: 'Hoje',
//        monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
//            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
//        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
//            'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
//        dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
//        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
//        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
//        weekHeader: 'Sm',
//        dateFormat: 'dd/mm/yy',
//        firstDay: 0,
//        isRTL: false,
//        showMonthAfterYear: false,
//        yearSuffix: ''};
//    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
//        autoclose: true
    });




    $(".peso-altura").maskMoney({thousands: '.', decimal: '.', affixesStay: true});
    $('.num-inteiro').maskMoney({decimal: ',', thousands: '.', precision: 0});
    $(".dinheiro-real").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', affixesStay: true});
}
);

     