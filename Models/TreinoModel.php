<?php

require_once 'ModelAbstract.php';

class TreinoModel extends ModelAbstract {

    private $trenId = null;
    private $trenSeq = null;
    private $atleId = null;
    private $tptrId = null;

    function __construct($trenId = null, $trenSeq = null, $atleId = null, $tptrId = null) {
        $this->trenId = $trenId;
        $this->trenSeq = $trenSeq;
        $this->atleId = $atleId;
        $this->tptrId = $tptrId;
    }

    function getTrenId() {
        return $this->trenId;
    }

    function getTrenSeq() {
        return $this->trenSeq;
    }

    function getAtleId() {
        return $this->atleId;
    }

    function getTptrId() {
        return $this->tptrId;
    }

    function setTrenId($trenId) {
        $this->trenId = $trenId;
    }

    function setTrenSeq($trenSeq) {
        $this->trenSeq = $trenSeq;
    }

    function setAtleId($atleId) {
        $this->atleId = $atleId;
    }

    function setTptrId($tptrId) {
        $this->tptrId = $tptrId;
    }

    public function checaAtributos($treinoView) {
        $atributosValidos = TRUE;

        if (empty(trim($this->trenSeq))) {
            $atributosValidos = FALSE;
            $treinoView->adicionaMensagensDeAlerta("O Campo Sequencia deve ser preenchido.");
        }

        if (empty(trim($this->atleId)) || $this->atleId == '-1') {
            $atributosValidos = FALSE;
            $treinoView->adicionaMensagensDeAlerta("O Campo Atleta deve ser selecionado.");
        }

        if (empty(trim($this->tptrId)) || $this->tptrId == '-1') {
            $atributosValidos = FALSE;
            $treinoView->adicionaMensagensDeAlerta("O Campo Tipo de Treino deve ser selecionado.");
        }

        return $atributosValidos;
    }

}
