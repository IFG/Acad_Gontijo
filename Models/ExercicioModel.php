<?php

require_once 'ModelAbstract.php';

class ExercicioModel extends ModelAbstract {

    private $exerId = null;
    private $exerNome = null;
    private $exerDescricao = null;
    private $tptrId = null;

    function __construct($exerId = null, $exerNome = null, $exerDescricao = null, $tptrId = null) {
        $this->exerId = $exerId;
        $this->exerNome = $exerNome;
        $this->exerDescricao = $exerDescricao;
        $this->tptrId = $tptrId;
    }

    function getExerId() {
        return $this->exerId;
    }

    function getExerNome() {
        return $this->exerNome;
    }

    function getExerDescricao() {
        return $this->exerDescricao;
    }

    function getTptrId() {
        return $this->tptrId;
    }

    function setExerId($exerId) {
        $this->exerId = $exerId;
    }

    function setExerNome($exerNome) {
        $this->exerNome = $exerNome;
    }

    function setExerDescricao($exerDescricao) {
        $this->exerDescricao = $exerDescricao;
    }

    function setTptrId($tptrId) {
        $this->tptrId = $tptrId;
    }

    public function checaAtributos($exercicioView) {
        $atributosValidos = TRUE;

        if (empty(trim($this->exerNome))) {
            $atributosValidos = FALSE;
            $exercicioView->adicionaMensagensDeAlerta("O Campo Nome deve ser preenchido.");
        }

        if (empty(trim($this->exerDescricao))) {
            $atributosValidos = FALSE;
            $exercicioView->adicionaMensagensDeAlerta("O Campo Descrição deve ser preenchido.");
        }

        if (empty(trim($this->tptrId)) || $this->tptrId == '-1') {
            $atributosValidos = FALSE;
            $exercicioView->adicionaMensagensDeAlerta("O Campo Tipo de Treino deve ser selecionado.");
        }

        return $atributosValidos;
    }

}
