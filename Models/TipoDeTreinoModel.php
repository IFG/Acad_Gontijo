<?php

require_once 'ModelAbstract.php';

class TipoDeTreinoModel extends ModelAbstract {

    private $tptrId = null;
    private $tptrNome = null;
    private $tptrDescricao = null;

    function __construct($tptrId = null, $tptrNome = null, $tptrDescricao = null) {
        $this->tptrId = $tptrId;
        $this->tptrNome = $tptrNome;
        $this->tptrDescricao = $tptrDescricao;
    }

    function getTptrId() {
        return $this->tptrId;
    }

    function getTptrNome() {
        return $this->tptrNome;
    }

    function getTptrDescricao() {
        return $this->tptrDescricao;
    }

    function setTptrId($tptrId) {
        $this->tptrId = $tptrId;
    }

    function setTptrNome($tptrNome) {
        $this->tptrNome = $tptrNome;
    }

    function setTptrDescricao($tptrDescricao) {
        $this->tptrDescricao = $tptrDescricao;
    }

    public function checaAtributos($tipoDeTreinoView) {
        $atributosValidos = TRUE;

        if (empty(trim($this->tptrNome))) {
            $atributosValidos = FALSE;
            $tipoDeTreinoView->adicionaMensagensDeAlerta("O Campo Nome deve ser preenchido.");
        }

        if (empty(trim($this->tptrDescricao))) {
            $atributosValidos = FALSE;
            $tipoDeTreinoView->adicionaMensagensDeAlerta("O Campo Descrição deve ser preenchido.");
        }

        return $atributosValidos;
    }

}
