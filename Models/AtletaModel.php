<?php

require_once 'ModelAbstract.php';
require_once '../Classes/Cpf.php';
require_once '../Classes/DatasEHoras.php';

class AtletaModel extends ModelAbstract {

    private $atleId = null;
    private $atleNome = null;
    private $atleCpf = null;
    private $atleDataNascimento = null;
    private $atleSexo = null;
    private $atlePeso = null;
    private $atleAltura = null;
    private $atlePretensao = null;
    private $treiId = null;

    function __construct($atleId = null, $atleNome = null, $atleCpf = null, $atleDataNascimento = null, $atleSexo = null, $atlePeso = null, $atleAltura = null, $atlePretensao = null, $treiId = null) {
        $this->atleId = $atleId;
        $this->atleNome = $atleNome;
        $this->atleCpf = $atleCpf;
        $this->atleDataNascimento = $atleDataNascimento;
        $this->atleSexo = $atleSexo;
        $this->atlePeso = $atlePeso;
        $this->atleAltura = $atleAltura;
        $this->atlePretensao = $atlePretensao;
        $this->treiId = $treiId;
    }

    function getAtleId() {
        return $this->atleId;
    }

    function getAtleNome() {
        return $this->atleNome;
    }

    function getAtleCpf() {
        return $this->atleCpf;
    }

    function getAtleDataNascimento() {
        return $this->atleDataNascimento;
    }

    function getAtleSexo() {
        return $this->atleSexo;
    }

    function getAtlePeso() {
        return $this->atlePeso;
    }

    function getAtleAltura() {
        return $this->atleAltura;
    }

    function getAtlePretensao() {
        return $this->atlePretensao;
    }

    function getTreiId() {
        return $this->treiId;
    }

    function setAtleId($atleId) {
        $this->atleId = $atleId;
    }

    function setAtleNome($atleNome) {
        $this->atleNome = $atleNome;
    }

    function setAtleCpf($atleCpf) {
        $this->atleCpf = $atleCpf;
    }

    function setAtleDataNascimento($atleDataNascimento) {
        $this->atleDataNascimento = $atleDataNascimento;
    }

    function setAtleSexo($atleSexo) {
        $this->atleSexo = $atleSexo;
    }

    function setAtlePeso($atlePeso) {
        $this->atlePeso = $atlePeso;
    }

    function setAtleAltura($atleAltura) {
        $this->atleAltura = $atleAltura;
    }

    function setAtlePretensao($atlePretensao) {
        $this->atlePretensao = $atlePretensao;
    }

    function setTreiId($treiId) {
        $this->treiId = $treiId;
    }

    public function checaAtributos($atletaView) {
        $atletaAdo = new AtletaAdo();
        $atributosValidos = TRUE;

        if (empty(trim($this->atleNome))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Nome deve ser preenchido.");
        }

        if (empty(trim($this->atleCpf))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo CPF deve ser preenchido.");
        } else if (!CPF::validaCPF($this->atleCpf)) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo CPF deve ser preenchido corretamente.");
            /* Esta condição abaixo foi feita para verificar se o CPF já existe, se ele não existe retorna 0
              Se já existe, retorna o objeto */
        } else if (!(($atletaAdo->buscaCpfExiste($this->atleCpf)) === 0)) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("Este CPF já está cadastrado no Sistema.");
        }

        if (empty(trim($this->atleDataNascimento))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Data de Nascimento deve ser preenchido.");
        } else if (!DatasEHoras::checaData($this->atleDataNascimento)) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Data de Nascimento deve ser preenchido corretamente.");
        }

        if (empty(trim($this->atleSexo))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Sexo deve ser preenchido.");
        }

        if (empty(trim($this->atlePeso))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Peso deve ser preenchido.");
        }

        if (empty(($this->atleAltura))) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Altura deve ser preenchido.");
        }

        if (empty(trim($this->atlePretensao)) || $this->atlePretensao == -1) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Pretensão deve ser selecionado.");
        }

        if (empty(trim($this->treiId)) || $this->treiId == -1) {
            $atributosValidos = FALSE;
            $atletaView->adicionaMensagensDeAlerta("O Campo Treinador deve ser selecionado.");
        }

        return $atributosValidos;
    }

}
