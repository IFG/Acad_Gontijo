<?php

require_once 'ModelAbstract.php';

class TreinoExercicioModel extends ModelAbstract {

    private $trexId = null;
    private $trexRepeticao = null;
    private $trexSerie = null;
    private $trexTempo = null;
    private $exerId = null;
    private $trenId = null;

    function __construct($trexId = null, $trexRepeticao = null, $trexSerie = null, $trexTempo = null, $exerId = null, $trenId = null) {
        $this->trexId = $trexId;
        $this->trexRepeticao = $trexRepeticao;
        $this->trexSerie = $trexSerie;
        $this->trexTempo = $trexTempo;
        $this->exerId = $exerId;
        $this->trenId = $trenId;
    }

    function getTrexId() {
        return $this->trexId;
    }

    function getTrexRepeticao() {
        return $this->trexRepeticao;
    }

    function getTrexSerie() {
        return $this->trexSerie;
    }

    function getTrexTempo() {
        return $this->trexTempo;
    }

    function getExerId() {
        return $this->exerId;
    }

    function getTrenId() {
        return $this->trenId;
    }

    function setTrexId($trexId) {
        $this->trexId = $trexId;
    }

    function setTrexRepeticao($trexRepeticao) {
        $this->trexRepeticao = $trexRepeticao;
    }

    function setTrexSerie($trexSerie) {
        $this->trexSerie = $trexSerie;
    }

    function setTrexTempo($trexTempo) {
        $this->trexTempo = $trexTempo;
    }

    function setExerId($exerId) {
        $this->exerId = $exerId;
    }

    function setTrenId($trenId) {
        $this->trenId = $trenId;
    }

    public function checaAtributos($treinoExercicioView) {
        $atributosValidos = TRUE;

        if ((empty(trim($this->trexTempo))) && ((empty(trim($this->trexRepeticao))) && (empty(trim($this->trexSerie))))) {
            $atributosValidos = FALSE;
            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo Tempo deve ser preenchido.");
            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo Série deve ser preenchido.");
            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo Repetição deve ser preenchido.");
        }

//        if ((empty(trim($this->trexTempo))) && (empty(trim($this->trexRepeticao))) && (empty(trim($this->trexSerie)))) {
//            $atributosValidos = FALSE;
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Tempo' deve ser preenchido.");
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Série' deve ser preenchido.");
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Repetição' deve ser preenchido.");
//        }
//
//        if (empty(trim($this->trexTempo))) {
//            $atributosValidos = FALSE;
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Tempo' deve ser preenchido.");
//        } else if ((empty(trim($this->trexRepeticao))) && (empty(trim($this->trexSerie)))) {
//            $atributosValidos = FALSE;
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Série' deve ser preenchido.");
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Repetição' deve ser preenchido.");
//        }
//        if (empty(trim($this->trexRepeticao))) {
//            $atributosValidos = FALSE;
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Repetição' deve ser preenchido.");
//        }
//
//        if (empty(trim($this->trexTempo))) {
//            $atributosValidos = FALSE;
//            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo 'Tempo' deve ser preenchido.");
//        }

        if (empty(trim($this->trenId)) || $this->trenId == '-1') {
            $atributosValidos = FALSE;
            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo Treino deve ser selecionado.");
        }

        if (empty(trim($this->exerId)) || $this->exerId == '-1') {
            $atributosValidos = FALSE;
            $treinoExercicioView->adicionaMensagensDeAlerta("O Campo Exercício deve ser selecionado.");
        }



        return $atributosValidos;
    }

}
