<?php

require_once 'ModelAbstract.php';

class UsuarioModel extends ModelAbstract {

    private $usuaId = null;
    private $usuaLogin = null;
    private $usuaNome = null;
    private $usuaSenha = null;
    private $usuaNivel = null;

    function __construct($usuaId = null, $usuaLogin = null, $usuaNome = null, $usuaSenha = null, $usuaNivel = null) {
        $this->usuaId = $usuaId;
        $this->usuaLogin = $usuaLogin;
        $this->usuaNome = $usuaNome;
        $this->usuaSenha = $usuaSenha;
        $this->usuaNivel = $usuaNivel;
    }
    
    function getUsuaId() {
        return $this->usuaId;
    }
    
    function getUsuaLogin() {
        return $this->usuaLogin;
    }

    function getUsuaNome() {
        return $this->usuaNome;
    }

    function getUsuaSenha() {
        return $this->usuaSenha;
    }

    function getUsuaNivel() {
        return $this->usuaNivel;
    }

    function setUsuaId($usuaId) {
        $this->usuaId = $usuaId;
    }
    
    function setUsuaLogin($usuaLogin) {
        $this->usuaLogin = $usuaLogin;
    }
    
    function setUsuaNome($usuaNome) {
        $this->usuaNome = $usuaNome;
    }

    function setUsuaSenha($usuaSenha) {
        $this->usuaSenha = $usuaSenha;
    }

    function setUsuaNivel($usuaNivel) {
        $this->usuaNivel = $usuaNivel;
    }

    public function checaAtributos($loginView) {
        
    }

}
