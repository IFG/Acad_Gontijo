<?php

require_once 'ModelAbstract.php';

class TreinadorModel extends ModelAbstract {

    private $treiId = null;
    private $treiNome = null;
    private $treiSexo = null;

    function __construct($treiId = null, $treiNome = null, $treiSexo = null) {
        $this->treiId = $treiId;
        $this->treiNome = $treiNome;
        $this->treiSexo = $treiSexo;
    }

    function getTreiId() {
        return $this->treiId;
    }

    function getTreiNome() {
        return $this->treiNome;
    }

    function getTreiSexo() {
        return $this->treiSexo;
    }

    function setTreiId($treiId) {
        $this->treiId = $treiId;
    }

    function setTreiNome($treiNome) {
        $this->treiNome = $treiNome;
    }

    function setTreiSexo($treiSexo) {
        $this->treiSexo = $treiSexo;
    }

    public function checaAtributos($treinadorView) {
        $atributosValidos = TRUE;

        if (empty(trim($this->treiNome))) {
            $atributosValidos = FALSE;
            $treinadorView->adicionaMensagensDeAlerta("O Campo Nome deve ser preenchido.");
        }
        
        if (empty(trim($this->treiSexo))) {
            $atributosValidos = FALSE;
            $treinadorView->adicionaMensagensDeAlerta("O Campo Sexo deve ser preenchido.");
        }

        return $atributosValidos;
    }

}
